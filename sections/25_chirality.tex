\subsection{Spinor chirality}

In the previous sections, we have defined space spinors as elements of the representation space of the geometric algebra of space. To do so, we had to choose a specific representation first - the Pauli matrix representation. We can now ask ourselves whether this definition of spinors is independent of the exact choice of representation matrices. To answer this question, we investigate the possible choices for a complex 2-by-2 matrix representation of $\Cl(3)$, and whether they have an effect on the transformation properties of spinors.

Formally, a representation $\rho$ is defined as a map that maps elements of a group to an endomorphism over some linear space (i.e. to a matrix):
\begin{align}
	\rho: G &\to \End(L) \\
	      g &\mapsto \rho(g).
\end{align}
It should fulfill the self-consistency requirement $\rho(g \cdot h) = \rho(g) \rho(h)$. In the context of Clifford algebra representation theory, we say that two representations $\rho$, $\rho'$ are \textit{similar} iff they can be mapped onto each other with a rotor transformation:
\begin{align}
	\rho' = R \rho \Tilde{R}.
\end{align}
In this case, the difference between these two representations reduces to the choice of a spinor basis. For instance, the Pauli matrix representation has $\{\rh{+}, \rh{-}\}$ as its basis for a spinor space, and we could transform this basis by a $90^\circ$ rotation along the $xz$ plane
\begin{align}
	R = \exp(\frac{e_{31}}{2} \pi/2) = \frac{1}{\sqrt{2}} (1 + e_{31})
\end{align}
to obtain the new basis
\begin{align}
	\{\grave{R} \rh{+}, \grave{R} \rh{-}\} = \left\{ \frac{1}{\sqrt{2}} \begin{pmatrix} 1 \\ -1 \end{pmatrix}, \frac{1}{\sqrt{2}} \begin{pmatrix} 1 \\ 1 \end{pmatrix} \right\},
\end{align}
corresponding to the representation where $e_1, e_2, e_3$ are represented by $P_z$, $P_y$ and $-P_x$ respectively. It is easy to see that the transformation properties of the spinors are unaffected by this change in representation - from a spinor viewpoint, choosing a different similar representation simply is a change in spinor basis.

Now, let $\rho$ be the Pauli matrix representation introduced in the last section. We will try to build another arbitrary representation $\rho'$ and see whether it is necessarily similar to $\rho$. The scalar of the geometric algebra needs to be mapped to the identity matrix by $\rho'$:
\begin{align}
	\rho'(1) = \rho(1) = \begin{pmatrix} 1 & 0 \\ 0 & 1 \end{pmatrix}.
\end{align}
Next, we have to choose the matrices representing the basis vectors $\rho'(\sigma^i)$. They need to square to $+1$ and anticommute with each other. As the space of complex 2-by-2 matrices that square to 1 is spanned by the identity matrix and the Pauli matrices, the $\rho'(e_i)$ have to be some orthonormal transformation of the Pauli matrices $\rho(e_i) = P_i$:
\begin{align}
	\rho'(e_i) = M\indices{^j_i} \ \rho(e_j), \ \ \ M \in \O(3).
\end{align}
If $M \in \SO(3)$, we can find a rotor $R$ such that $\rho' = R \rho \Tilde{R}$. However, if the transformation involves a parity flip ($\det(M) = -1$), this is not possible - the group of rotors only covers the \textit{special} orthogonal group. Therefore, there are two equivalence classes of representations - those who are similar to $\rho(\sigma^i)$, and those who are similar to e.g. $-\rho(\sigma^i)$. The former ones are conventionally called \textit{right-handed}, while the latter are called \textit{left-handed}. Spinors transforming under them are called right-handed and left-handed spinors respectively.

In order to cover both possibilities, we will define the \textit{left-handed} Pauli matrix representation
\begin{align}
	\acute{e}_i = - P_i
\end{align}
related to the standard, \textit{right-handed} Pauli matrix representation $\grave{\sigma}^i$ by a parity flip.

We will denote the right-handed spinors/Pauli matrix representations with grave accents, and left-handed ones with acute accents:
\begin{align}
	\rh{\psi} &\mapsto \grave{R} \rh{\psi} \ \ \text{(right-handed)} \\
	\lh{\psi} &\mapsto \acute{R} \lh{\psi} \ \ \text{(left-handed)}.
\end{align}

However, this distinction is largely irrelevant in three dimensions - the representation of any bivector $e_{ij}$ is unaffected by this parity flip, as
\begin{align}
	e_{ij} = e_{i} \wedge e_j = (-e_i) \wedge (-e_j)
\end{align}

Therefore, a 3D rotor $R$ will remain the same upon a parity flip, and the transformation properties of right-handed and left-handed space spinors are the same. However, we need to take care of a minus sign in the calculation of the spin vector. Consider for instance
\begin{align}
	\ket{\psi} &= \begin{pmatrix} 1 \\ 0 \end{pmatrix} \\
	\ket{\psi} \bra{\psi} &= \begin{pmatrix} 1 \\ 0 \end{pmatrix} \begin{pmatrix} 1 & 0 \end{pmatrix} = \begin{pmatrix} 1 & 0 \\ 0 & 0 \end{pmatrix} \\
	\langle \ket{\psi} \bra{\psi} \rangle_1 &= \frac{1}{2} P_z.
\end{align}
The latter matrix corresponds to $e_z/2$ when we interpret $\psi$ as a right-handed spinor $\rhpsi$, but to $-e_z/2$ when we interpret $\psi$ as a left-handed spinor $\lhpsi$.

We can easily convert right-handed and left-handed representations into each other with a parity flip $P^i \mapsto -P^i$, as done above. Now, we can ask ourselves whether it is also possible to convert right-handed into left-handed spinors and vice versa. It turns out that this is possible: The map
\begin{align}
	\lh{\psi'} &= \epsilon \rh{\psi}^* \label{eq:2_chirality_conversion}
\end{align}
with the matrix
\begin{align}
	\epsilon = \begin{pmatrix} 0 & 1 \\ -1 & 0 \end{pmatrix}
\end{align}
converts a right-handed spinor $\rh{\psi}$ into a left-handed spinor $\lh{\psi'}$ such that the downprojected multivector stays the same.\footnote{This map is not even unique - we can also add a minus sign to $\epsilon$.} This is easy to show: When performing a parity flip, the multivector representation $\grave{M} = \rhpsi \rhpsi^\dag$ will transform as:
\begin{align}
	\grave{M} = \rh{\psi} \rhc{\psi} \mapsto \lh{\psi'} \lhc{\psi'} = \epsilon \rh{\psi}^* \rhc{\psi}^* \epsilon^T = \epsilon \grave{M'}^* \epsilon^T
\end{align}
In fact, we can see that for all grades of $M$:
\begin{align}
	\epsilon \grave{1}^* \epsilon^T &= \acute{1} \\
	\epsilon \grave{\sigma}_i \epsilon^T &= \acute{\sigma}_i \\
	\epsilon (\grave{I} \grave{\sigma}_i)^* \epsilon^T &= \acute{I} \acute{\sigma}_i \\
	\epsilon \grave{I}^* \epsilon^T &= \acute{I}
\end{align}
such that:
\begin{align}
	\epsilon \grave{M}^* \epsilon^T = \acute{M}.
\end{align}
In other words, we have converted a right-handed spinor $\rh{\psi}$ into a left-handed spinor $\lh{\psi}$. The former down-projects to a right-handed representation matrix $\Rh{M}$ of a multivector, while the latter down-projects to a left-handed one $\Lh{M}$. Both matrices represent the same multivector.

We see that we can effortlessly convert between right-handed and left-handed space spinors. Hence, this distinction is almost always ignored in introductory quantum mechanics, and the right-handed space matrix representation is conventionally used for space spinors. This distinction will, however, turn out to be relevant in the treatment of spacetime spinors, as we will see in the next section.

