\subsection{Even multivector formalism}

The even multivector formalism for describing spinors and spinor fields is due to David Hestenes. Its basic premise is not to calculate with spinors themselves, but with even multivectors transforming a chosen ``reference spinor'' to the spinor in question, similar to rotor mechanics in geometric algebra. In the following, we will give a superficial overview of this formalism and how it relates to ours.

\subsubsection{Space spinors}
Consider, for instance, a space spinor $\rh{\psi}$. We can always find a 3D rotor $\psi$ that rotates a given reference spinor, say $\rh{+}$, onto $\rh{\psi}$:
\begin{align}
	\rh{\psi} = \Rh{\psi} \rh{+}.
\end{align}
This is easy to show: Let
\begin{align}
	\rh{\psi} = \begin{pmatrix} a + b_3 i \\ -b_2 + b_1i \end{pmatrix}
\end{align}
be a parametrization of $\rh{\psi}$. We are looking for a two-by-two matrix $\Rh{\psi}$ that maps
\begin{align}
	\rh{+} = \begin{pmatrix} 1 \\ 0 \end{pmatrix}
\end{align}
onto $\rh{\psi}$. The left column of this matrix has to be equal to $\rh{\psi}$. The Pauli representation of a 3D rotor is a linear superposition of the identity matrix and $i P_i$. Therefore, $\Rh{\psi}$ has to be
\begin{align}
	\Rh{\psi} = \begin{pmatrix} a + b_3i & b_2 + b_1i \\ -b_2 + b_1i & a - b_3i \end{pmatrix},
\end{align}
or, going out of the matrix representation,
\begin{align}
	\rh{\psi} = \begin{pmatrix} a + b_3 i \\ -b_2 + b_1 i \end{pmatrix} \longleftrightarrow R = a + b_i I \sigma_i,
\end{align}
where $a^2 = - \sum b_i^2$. This is the rotor that rotates our reference spinor $\rh{+}$ onto $\rh{\psi}$. We are going to call such a rotor $\psi$ the even multivector representation\footnote{Here, we do not use ``representation'' in the representation-theoretic sense, but simply in the sense of ``stand-in for another object''} of $\ket{\psi}$. Hestenes calls objects like $\psi$ ``spinors'', but we wish to emphazise that $R(\psi)$ is not a spinor, but a rotor acting on spinors - if $\psi$ actually were an even multivector in the geometrical sense, it would transform under a two-sided transformation law. Instead, applied to this ``reference rotor'' scheme, the spinor transformation law
\begin{align}
	\rh{\psi} \mapsto R \rh{\psi}
\end{align}
maps to
\begin{align}
	\psi \mapsto R \psi.
\end{align}
This shows why it is a stretch to equate spinors and rotors - under a transformation, we would normally expect a multivector $\psi$ to transform as
\begin{align}
	\psi \mapsto R \psi \Tilde{R}.
\end{align}

Down-projecting our reference space spinor $\rh{+}$ will yield a 3D ``reference down-projection'' $M_0 = \rh{+} \rhc{+}$; in this case simply
\begin{align}
	M_0 = \rh{+} \rhc{+} = 1 + e_3.
\end{align}
If we want to find the down-projection between two arbitrary space spinors $\rh{\psi}, \rh{\phi}$, we can use $M_0$:
\begin{align}
	\rh{\psi} \rhc{\phi} = \psi \rh{+} \rhc{+} \Tilde{\phi} = \psi M_0 \Tilde{\phi}
\end{align}
This looks like the two-sided rotor transformation law - and in fact, it is, if $\rh{\psi} = \rh{\phi}$. If this is not the case, there is ``mixing'' between the grades. For instance, let
\begin{align}
	\rh{\psi} = \begin{pmatrix} 1 \\ 1 \end{pmatrix} \quad &\longleftrightarrow \quad \psi = \frac{1}{\sqrt{2}} (1 - e_{31}) \\
	\rh{\psi} = \begin{pmatrix} 1 \\ -1 \end{pmatrix} \quad &\longleftrightarrow \quad \phi = \frac{1}{\sqrt{2}} (1 + e_{31}).
\end{align}
Then,
\begin{align}
	\rh{\psi} \rhc{\phi} &= \psi M_0 \Tilde{\phi} \\
			     &= \frac{1}{2} (1 - e_{31}) (1 + e_3) (1 - e_{31}) \\
			     &= e_z - I e_x,
\end{align}
We see that this down-projection is a mixture of a vector and pseudovector part.\footnote{Interestingly, this is the basis state for circularly polarized light propagating in the $y$ direction. This makes perfect sense - a spin-1 photon hitting an electron with spin-down will turn it into spin-up. We can therefore, in a sense, say that the raising and lowering operators for spin-1/2 are the chiral representation of the circularly polarized states of light.


\subsubsection{Chiral spinors}
It is not straightforward to extend this scheme to chiral spinors for a simple reason: The space of STA rotors has six degrees of freedom (one scalar, three rotations and three boosts, minus the normalization constraint), but a single chiral spinor only has four (two complex numbers). Therefore, we cannot describe an arbitrary chiral spinor $\rh{\psi}$ by a normal rotor applied to a reference spinor $\rh{+}$, as this would not be a bijective map.

We remind ourselves that chiral spinors downproject to null vectors. Therefore, it makes intuitive sense to represent chiral spinors by the ``infinitely large boosts''
\begin{align}
	P_R &= \frac{1 + \gamma^{30}}{2}, \\
	P_L &= \frac{1 - \gamma^{30}}{2}.
\end{align}
These are not boosts in the strict sense, as they have no inverse. However, in a certain sense, they behave like rotors boosting objects from timelike to null. For instance, $P_R$ will project the timelike vector $\gamma_0$ to:
\begin{align}
	P_R \gamma_0 \Tilde{P}_R = \frac{1}{2} \left( \gamma_0 + \gamma_3 \right),
\end{align}
a null vector.

Analogously to space spinors, we now define the reference chiral spinors to be
\begin{align}
	\rh{+} &= \begin{pmatrix} 1 \\ 0 \end{pmatrix}, \\
	\lh{+} &= \begin{pmatrix} 0 \\ 1 \end{pmatrix}.
\end{align}
We observe that
\begin{align}
	P_R \rh{+} &= \rh{+} \\
	P_L \lh{+} &= \lh{+}.
\end{align}
This allows us to define the even multivector representation of these two chiral spinors to be:
\begin{align}
	\rh{+} \quad &\longleftrightarrow \quad P_R, \\
	\rh{-} \quad &\longleftrightarrow \quad P_L.
\end{align}
Under boosts, these even multivectors behave like the original spinors:
\begin{align}
	\exp(\Rh{\gamma}_{30} \eta / 2) \rh{\psi} = e^{\eta/2} \rh{\psi} \quad \longleftrightarrow \quad \exp{\gamma_{30} \eta/2} P_L = e^{\eta/2} P_R.
\end{align}
We can now apply additional rotations to them to represent arbitrary chiral spinors other than $e^{\eta/2} \rh{+}, e^{\eta/2} \lh{+}$. In fact, if we naïvely treat a chiral spinor $\rh{\psi}$ as a space spinor and find a 3D rotor representation $\psi_{\text{space}}$ for it as outlined in the previous section, the corresponding chiral even multivector representation will be
\begin{align}
	\psi = \psi_{\text{space}} P_R 
\end{align}
for right-chiral spinors, and
\begin{align}
	\psi = \psi_{\text{space}} P_L
\end{align}
for left-chiral spinors.

\subsubsection{Bichiral spinors}

Now, we can ask ourselves whether we can join two chiral spinors into a bichiral spinor as done before. The answer is yes - the multivectors $P_R, P_L$ which we used to bring our chiral spinors into boostable form are also a pair of disjoint projectors:
\begin{align}
	P_R^2 &= P_R \\
	P_L^2 &= P_L \\
	P_R P_L &= P_L P_R = 0
\end{align}

This means that we can join two chiral spinor representations together into a bichiral spinor representation:
\begin{align}
	\psi = \psi_L + \psi_R
\end{align}
No information is lost, as we can always project out $\psi_L$ and $\psi_R$ again:
\begin{align}
	\psi_R &= \psi P_R \\
	\psi_L &= \psi P_L.
\end{align}
These are the equivalent of the chiral projectors in the even multivector formalism.

In fact, the analogy between rotor mechanics and the even multivector formalism for spinors can be pushed even further - $\psi$ is an even multivector that will transform the reference spinor
\begin{align}
	\ket{u(\gamma^0, +)} = \begin{pmatrix} 1 \\ 0 \\ 1 \\ 0 \end{pmatrix}
\end{align}
into the bichiral spinor $\ket{\psi}$ it is representing:
\begin{align}
	\ket{\psi} = \psi \ket{u(\gamma_0, +)}.
\end{align}

If $\ket{\psi}$ represents an antiparticle, we add in an $I$ to the $\psi$ to represent the charge conjugation. Therefore, in total, we can decompose $\psi$ into a scalar $\rho^{1/2}$, a pseudoscalar exponential $e^{I\beta/2}$ and a normalized space-time rotor $R$:
\begin{align}
	\psi = \rho^{1/2} e^{I \beta / 2} R
\end{align}


Constructing observables is completely analogous to the space spinor case. The reference bichiral spinor down-projects to
\begin{align}
	M_0 := \ket{u(\gamma_0, +)}\bra{u(\gamma_0, +)} &= (1 + \gamma_0)(1 + i \gamma_{12}) \\
							&= 1 + \gamma_0 + i \gamma_{12} + i \gamma_{012},
\end{align}
such that the down-projection of two arbitraty bichiral spinors $\ket{\psi}$, $\ket{\phi}$ is
\begin{align}
	\ket{\psi} \bra{\phi} &= \psi \ket{u(\gamma_0, +)} \bra{u(\gamma_0, +)} \Tilde{\phi} \\
			      &= \psi M_0 \Tilde{\phi} = \psi (1 + \gamma_0 + i \gamma_{12} + i \gamma_{012}) \Tilde{\phi}
\end{align}
Again, if $\ket{\psi} = \ket{\phi}$, this is just the rotor transformation law applied to the reference spinor down-projection, analogously to how we derived the possible solutions of the bichiral fermion equation earlier:
\begin{align}
	\ket{\psi} \bra{\psi} &= \psi M_0 \Tilde{\psi} \\
	\Rightarrow \expval{ \ket{\psi} \bra{\psi} }_0 &= \psi \Tilde{\psi} \\
	\expval{ \ket{\psi} \bra{\psi} }_1 &= \psi \gamma_0 \Tilde{\psi} \\
	\expval{ \ket{\psi} \bra{\psi} }_2 &= i \psi \gamma_{12} \Tilde{\psi} \label{eq:4_bivector_obs} \\
	\expval{ \ket{\psi} \bra{\psi} }_3 &= i \psi \gamma_{012} \Tilde{\psi}
\end{align}
It is one of Hestenes' stated aims to remove the need for imaginary numbers in spinor algebra. Considering that $i$ is the eigenvalue of the operator $\gamma^{12}$ with respect to the reference spinor $\ket{u(\gamma^0, +)}$, we can replace all the imaginary units in even multivectors being applied to $\ket{u{\gamma^0, +}}$ with a $\gamma^{12}$ on the right side:
\begin{align}
	i \psi \longrightarrow \psi \gamma^{12}.
\end{align}
This programme of ``realification'' also includes an ad-hoc removal of the imaginary units in the observable grades (\ref{eq:4_bivector_obs}) etc, similar to how the Dirac bilineals of grade 2 and higher in the classical formalism are  conventionally defined with extra factors of $i$.

\subsubsection{Operators}
So far, we have only considered rotors acting on the even multivector representation of the spinors, and down-projection of spinors in this representation. However, we also frequently have to act on the spinor with odd multivectors - for instance in the following form of the bichiral fermion equation:
\begin{align}
	i \partial \ket{\psi} = m \ket{\psi}.
\end{align}
Naïvely applying a vector operator like $\partial$ to $\ket{\psi}$ would yield an odd multivector, which is not an element of the even subalgebra spinor representation anymore. Hence, the operator application law needs to look differently in this formalism.
 
Finding it is fairly straightforward. Let $A$ be a vector, and $\psi$ the even subalgebra representation of the spinor $\ket{\psi}$. Then, we wish to find the even multivector $\psi'$ representing $\ket{\psi'} = A \ket{\psi}$. It should fulfill:
\begin{align}
	\expval{\psi' \phi} &= \expval{A \ket{\psi} \bra{\phi} } \\
				&= A \cdot \expval{ \ket{\psi} \bra{\phi}}_1 \\
				&= A \cdot \expval{ \psi \gamma_0 \Tilde{\phi}}
\end{align}
for any placeholder spinor $\ket{\phi}$. This requirement is equivalent to the requirement that $\psi'$ represent $\ket{\psi}$. It is obviously fulfilled by
\begin{align}
	\phi' = A \psi \gamma_0.
\end{align}
This can be said to be the law for applying vector (and, by extension, trivector) operators to an even multivector spinor representation $\psi$. Note that the addition of $\gamma_0$ ensures that $A \psi \gamma_0$ is an even multivector again. The exact choice of $\gamma_0$ stems from choosing a reference spinor whose vector grade down-projection is $\gamma_0$.


