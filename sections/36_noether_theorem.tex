\subsection{Noether's theorem and observables of bichiral fermions}
The various grades of the down-projection of a bichiral spinor $\ket{\psi}$ all correspond to physical observables of the fermion. To show this, we are going to derive the expressions for the electromagnetic current and spin current of the bichiral field and relate them to the grades of $\ket{u} \bra{u}$.

Let $T$ be a symmetry of a bichiral spinor field:
\begin{align}
	\ket{\psi} &\mapsto \ket{\psi} + \epsilon T \ket{\psi} \\
	\bra{\psi} &\mapsto \bra{\psi} + \epsilon \bra{\psi} \Tilde{T},
\end{align}
where $T$ denotes the symmetry generator and $\Tilde{T}$ its STA reverse. The action $\mathcal{L}(\ket{\psi}, \partial \ket{\psi})$ should stay invariant up to a divergence $\partial \cdot W$:
\begin{align}
	\delta \mathcal{L} &= \pdv{\mathcal{L}}{\ket{\psi}} \delta \ket{\psi} + \pdv{\mathcal{L}}{(\partial \ket{\psi})} \delta \dot{\partial} \dot{\ket{\psi}} + \text{b.c.} = \partial \cdot W \\
			   &= \underbrace{\pdv{\mathcal{L}}{\ket{\psi}} \delta \ket{\psi} - \left( \pdv{\mathcal{L}}{(\partial \ket{\psi})} \right)^\cdot \dot{\partial} \delta \ket{\psi}}_{\text{$=0$ because of Euler-Lagrange equation}} + \left( \pdv{\mathcal{L}}{(\partial \ket{\psi})} \right)^\cdot \dot{\partial} \delta \dot{\ket{\psi}} + \text{b.c.} = \partial \cdot W \\
	\Rightarrow 0 &= \partial_\mu \left(\pdv{\mathcal{L}}{(\partial \ket{\psi})} \gamma^\mu T \ket{\psi} + \bra{\psi} \Tilde{T} \gamma^\mu \pdv{\mathcal{L}}{(\partial \bra{\psi})} - W^\mu\right).
\end{align}
where ``b.c.'' denotes the bichiral conjugate of the preceding terms, i.e. bras and kets flipped and STA reverses applied to all multivector expressions.

It follows that the conserved current is:
\begin{align}
	j^\mu = \pdv{\mathcal{L}}{(\partial \ket{\psi})} \gamma^\mu T \ket{\psi} + \bra{\psi} \Tilde{T} \gamma^\mu \pdv{\mathcal{L}}{(\partial \bra{\psi})} - W^\mu.
\end{align}
For a bichiral fermion, we have
\begin{align}
	\mathcal{L} &= \expval{\left(i\partial - m\right) \ket{\psi} \bra{\psi} } \\
	\pdv{\mathcal{L}}{(\partial \ket{\psi})}\ket{\phi} &= \expval{i \ket{\phi} \bra{\psi}}, \\
	\bra{\phi} \pdv{\mathcal{L}}{(\partial \bra{\psi})} &= \expval{-i \ket{\psi} \bra{\phi}},
\end{align}
where $\ket{\phi}$ denotes a placeholder bichiral spinor. Thus, Noether's theorem for bichiral fields reads:
\begin{align}
	j^\mu = \expval{ -i\frac{\gamma^\mu T - \Tilde{T} \gamma^\mu}{2} \ket{\psi} \bra{\psi}} - W^\mu,
\end{align}
where the generator $T$ acts on $\ket{\psi}$, and $\Tilde{T}$ on $\bra{\psi}$. We have inserted a factor of $-1/2$ to match the usual conventions.

\subsubsection{Electromagnetic U(1) current}
For the U(1) gauge transform, the symmetry generator is defined as
\begin{align}
	T \ket{\psi} &= i q \ket{\psi} \\
	\bra{\psi} \Tilde{T} &= -iq \bra{\psi}
\end{align}
where $q$ is the electric charge of the field. This leaves $\mathcal{L}$ invariant, so we have $W = 0$. We derive the corresponding Noether current:
\begin{align}
	j^\mu &= \expval{ -i\frac{\gamma^\mu T - \Tilde{T} \gamma^\mu}{2} \ket{\psi} \bra{\psi}} - W^\mu \\
	      &= \expval{-i \frac{\gamma^\mu i q - (-iq) \gamma^\mu}{2} \ket{\psi} \bra{\psi}} \\
	      &= q\expval{ \gamma^\mu \ket{\psi} \bra{\psi}},
\end{align}
or in other words:
\begin{align}
	j = q \expval{\ket{\psi} \bra{\psi}}_1
\end{align}
We see that the vector part of $q \ket{\psi} \bra{\psi} = (q + j)(1 + is)$ corresponds to the EM current flowing at that point. It is related to the scalar part by the bichiral fermion equation,
\begin{align}
	(p - m) q \ket{\psi} \bra{\psi} &= 0 \\
	\Rightarrow qU = j.
\end{align}

We see that the scalar part of $q \ket{\psi} \bra{\psi}$ can be interpreted as the charge $q$ of the field at that point, and the vector part as the current $j$. The bichiral fermion equation ensures that the relation $qU = j$ holds.

\subsubsection{Spin current}
If we want to derive the current of angular momentum around a bivector $B$, the corresponding transformation law would be
\begin{align}
	T\ket{\psi} = \frac{B}{2} \ket{\psi} - \left(\frac{B}{2} \times x\right) \cdot \partial\ket{\psi}.
\end{align}
For now, we are only interested in the spin current part, so we discard the orbital angular momentum part and consider the transformation\footnote{Again, keep in mind that this is not a symmetry transformation anymore, so the corresponding current is not conserved.}
\begin{align}
	T \ket{\psi} &= B \ket{\psi} \\
	\bra{\psi} \Tilde{T} &= - \bra{\psi} B.
\end{align}
The corresponding ``Noether current'', aka the spin current, is:
\begin{align}
	S^\mu(B) &= \expval{ -i \frac{\gamma^\mu \frac{B}{2} + \frac{B}{2} \gamma^\mu}{2} \ket{\psi} \bra{\psi}} \\
		 &= \frac{1}{2} \expval{ -i \gamma^\mu \wedge B \ket{\psi} \bra{\psi} }.
\end{align}
We see that calculating the $\mu$ component of the spin current around the plane of rotation $B$ entails taking the wedge product $\gamma^\mu \wedge B$ and extracting the trivector component of the down-projection multivector parallel to it. This suggests writing the spin current of a bichiral fermion as a trivector $S$ straightaway\footnote{In principle, the spin current does not have to be a trivector - it is also possible to write down an action in which the spin current can be parallel to the plane of the spin charge.}. For instance, a spin current along the $\gamma_{12}$ plane flowing in the $\gamma_0$ direction would be
\begin{align}
	S \propto \gamma_{012}.
\end{align}
This spin current is given by:
\begin{align}
	S = -\frac{i}{2} \expval{\ket{\psi} \bra{\psi}}_3
\end{align}
i.e. the imaginary trivector component of the down-projection of $\ket{\psi}$ times 1/2. Analogously to the EM current, it is linked to the bivector component by the bichiral equation, such that:
\begin{align}
	U \expval{\ket{\psi} \bra{\psi}}_2 = \expval{\ket{\psi} \bra{\psi}}_3
\end{align}
We will therefore call the bivector part the ``spin charge'' of the fermion - then, the spin current is given by the spin charge bivector wedged with the four-velocity. Again, when we say ``spin charge'', we do not mean the timelike component of the spin current, but the generator of the rotation that is an eigenbivector to the spinor at hand. It is unfortunate that our current terminology frequently does not distinguish between eigenvalues of symmetry generators and the timelike components of the associated conserved Noether current. This has led to confusion over the issue at hand - the spin current trivector we just defined is related to the concepts of helicity and the classical spin pseudovector\footnote{In order to obtain the classical spin vector $\mathbf{s}$, one simply takes the dual $s = IS$ of the spin current and performs a space-time split w.r.t the rest frame of the particle.}, but few (if any) authors properly distinguish between them.

\subsubsection{A note on antiparticles}
The scalar part of the down-projection multivector represents the electric charge of the bichiral field. It is identical for particles and antiparticles. However, antiparticles are negative-frequency solutions. The bichiral fermion equation requires that for antiparticles, the electric current $j^0 = qU$ points into the past - therefore, the charge density $j^0$ becomes negative for antiparticles.

Analogously, the bivector part of the down-projection multivector represents the spin charge of the fermion. It is flipped upon charge conjugation - however, the physical angular momentum density of the particle is given by the timelike component of the spin current. As mentioned, this is often a point of great confusion; especially as the commonly used language of ``spin'' does not distinguish between spin charge and the timelike component of the spin current at all. This can be especially confusing when discussing force fields coupling to the bivector and trivector components - for instance, the axial vector current present in discussions of the $V-A$ structure of the weak interaction is proportional to the spin current, while the magnetic spin-spin coupling is dependent on the spin charge.

The energy of antiparticles is another point where the underlying ambiguity in terminology mentioned in the last section has historically been a cause of confusion (e.g. \cite{thomson_antiparticles}). We have seen that antiparticles are negative-frequency solutions - this means that their eigenvalues with respect to the temporal translation symmetry operator (the Hamiltonian) are negative. In ``classical'' QM, this negative frequencies are interpreted as negative energies, as described by the Schrödinger equation $H\ket{\psi} = E \ket{\psi}$. This has led to some amount of confusion - after all, in experiments, we see that positrons produce particle showers just like electrons and hence add positive energy to the detector. Again, the solution to this apparent paradox is that the eigenvalues of the symmetry generators and the Noether current (the latter being the actual, conserved quantity here) are not the same. Energy-as-in-frequency and energy-as-in-Noether-current are two distinct concepts that just happen to coincide classically. The Noether current associated with temporal translation symmetry is the energy 4-current $T^{\mu0}$, the timelike component of the energy-momentum tensor $T^{\mu \nu}$. As the reader can verify for themself, the energy density $T^{00}$ is positive for antiparticles, and the paradox is resolved. Historically, this fuzziness in terminology and a lack of awareness about Noether's theorem have led to interpretations like the Feynman-Stückelberg interpretation of particles travelling back in time, or the Dirac sea interpretation of antiparticles being ``holes'' in an infinite condensate of particles.
