\subsection{Imagining higher-dimensional space}

Generally speaking, our brains are only equipped to understand three spatial dimensions. The space we live in has 3+1 dimensions, so we evolved the mental capability to mentally picture three-dimensional objects rotating and moving through spacetime. Things get a lot harder however when we try to add another spatial dimension and picture rotations along this additional axis. Although it is certainly easy to write down the vector space $\BR^{4}$ and the transformation matrix group $\SO(4)$ and perform calculations with them, it is very hard to intuitively picture spatial 4D vectors and the six possible rotations in that space.

A common method to develop an intuition for higher-dimensional space is to picture a world inhabited by two-dim--ensional creatures, as it is most notably done in the Victorian-era novel ``Flatland'' \cite{flatland}. These creatures would only have evolved the mental capability to picture two spatial dimensions and one axis of rotation. If a three-dimensional object were to pass through their two-dimensional world-plane, they would just see one slice of the object at a time. For instance, a sphere passing through their world-plane would be perceived as a circle suddenly appearing out of nowhere, getting larger until the equator of the sphere is reached, and then smaller again until the sphere completely leaves the world-plane. The two-dimensional creatures could easily picture the individual slices, but the three-dimensional structure of the object would be elusive to them.

\begin{figure}
	\centering
	\begin{tikzpicture}
		\draw (0, 0) -- (2, 0) -- (2, 2) -- (0, 2) -- cycle;
		\draw (2.4, 0.4) -- (2.4, 2.4) -- (0.4, 2.4);
		\draw[dashed] (0.4, 0.4) -- (2.4, 0.4);
		\draw[dashed] (0.4, 0.4) -- (0.4, 2.4);
		\draw[dashed] (0, 0) -- (0.4, 0.4);
		\draw (2, 0) -- (2.4, 0.4);
		\draw (2, 2) -- (2.4, 2.4);
		\draw (0, 2) -- (0.4, 2.4);
	\end{tikzpicture}
	\caption{A 3D cube projected down to 2D space. Its projection consists of two 2D cubes interlinked with each other.}
	\label{fig:3d_cube_2d}
\end{figure}

Another way for these two-dimensional creatures to get a picture for the nature of three-dimensional objects would be to consider the projection of them onto 2D space (see Figure \ref{fig:3d_cube_2d}). This method has the advantage that all points are simultaneously laid out in 2D space for a 2D creature to examine, but the 3D depth information is also lost and has to be supplemented by the imagination of the observer. Also, such a projection does not properly capture the transformation properties of the object under 3D rotations. If we project a cube down to a plane and then rotate the plane with the image of the cube on it, the result will be different from that obtained by first rotating the cube and then projecting it down on the plane.

In analogy to these considerations, we can now ask ourselves how we could intuitively picture 4D objects. The first possibility would be to consider a 4D object ``passing through'' our 3D space, in which case we would see one 3D slice at a time. Alternatively, we could discard the depth information of the fourth spatial axis and project the object down to 3D space. This could be done for e.g. a tesseract (4D cube) - in this case, we would see two interlinked 3D cubes, similar to how a 3D cube projected down to 2D consists of two interlinked squares.

In conclusion, there are various methods to supplement our geometric intuition to imagine 4D space, but each of them has drawbacks that need to be explicitly discussed. We will see that the situation is the same for spinors. There are two main approaches to picture spinors. The core point of this paper is the operation of ``down-projecting'' a pair of spinors into spin-1 vector space. The even multivector formalism consists of representing spinors as even multivectorsrelative to a ``reference spinor''. The advantages and disadvantages of the respective formalisms will be discussed at the end.


