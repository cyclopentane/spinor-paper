\subsection{Spinors and vectors}
% Two-sided rotor transformation law
Now that we have thoroughly discussed the relationship between vectors and quadratic forms, we go back to the geometric algebra formalism and remind ourselves that vectors transform under a two-sided rotor transformation law:
\begin{align}
	x \mapsto R x \Tilde{R}. \label{eq:vector rotor transformation}
\end{align}
In geometric algebra notation, a quadratic form can be written as
\begin{align}
	\beta: (v, w) \mapsto w \cdot \beta(v) = v \cdot \beta(w),
\end{align}
with the ``four-sided'' transformation law
\begin{align}
	(x \mapsto \beta(x)) \mapsto (x \mapsto R \ \beta(\Tilde{R} x R) \ \Tilde{R}),
\end{align}
as two rotors are equivalent to one rotation matrix. Roughly, a two-sided rotor transformation law corresponds to spin-1 objects, and a symmetric four-sided transformation law to spin-2 objects. The verbosity of this four-sided rotor transformation law was the reason for why we used the traditional matrix-vector formalism in the last section.

Given the two-sided rotor transformation law, vectors in geometric algebra are starting to look like quadratic forms in the matrix-vector formalism. We can now ask ourselves whether the two-sided rotor transformation law implies the existence of a spin-1/2 object that transforms under a one-sided rotor transformation law:
\begin{align}
	\psi \mapsto R \psi.
\end{align}
In fact, such objects exist. They are called \textit{spinors}, and spinors are to (multi)vectors roughly what vectors are to quadratic forms. In the following, we will deduce the main properties of spinors based on this analogy - essentially, we will try to retrace the path of the previous section backwards.

In order for our analogy to hold, we need to represent all multivectors in (\ref{eq:vector rotor transformation}) as matrices, similar to (\ref{eq:bilinear matrix transformation}). Thus, we need a representation of the geometric algebra of space $\Cl(3)$ spanned by the basis vectors $e_i$. A common choice for this is the $\Mat(2 \times 2, \BC)$ representation with the Pauli matrices:
\begin{align}
	P_1 &= \begin{pmatrix} 0 & 1 \\ 1 & 0 \end{pmatrix} \\
	P_2 &= \begin{pmatrix} 0 & -i \\ i & 0 \end{pmatrix} \\
	P_3 &= \begin{pmatrix} 1 & 0 \\ 0 & -1 \end{pmatrix}, \\
	\grave{e}_i &:= P_i.
\end{align}
We use the grave accent $\grave{a}$ to denote the Pauli matrix representation of a multivector $a$. The $\Mat(2 \times 2, \BC)$ representation of $\Cl(3)$ is surjective, i.e. every two-by-two complex matrix corresponds to a 3D multivector.

\subsubsection{Space spinors}

We now define the \textit{space spinors}\footnote{Conventionally called Pauli spinors, or simply ``spin states'' in introductory QM.} $\rh{\psi} \in \grave{S}$ as complex two-element column matrices (``vectors''):
\begin{align}
	\rh{\psi} &= \begin{pmatrix} a \\ b \end{pmatrix}, \ \ a, b \in \BC.
\end{align}
We use bra-ket notation for spinors to distinguish between spinors and multivectors. However, the reader should not view spinors as quantum-mechanical states and the multivectors as operators - in principle, spinors are a purely classical and geometric formalism. We are \textit{not} doing quantum mechanics - the bra-ket notation simply is a convenient notation to distinguish between spinors and multivectors.

For space spinors, we define:
\begin{align}
	\rhc{\psi} := \rh{\psi}^\dag.
\end{align}
Contrary to what we are used to from quantum mechanics and the classical formalism, the bra $\bra{\psi}$ will \textit{not} always denote the hermitean conjugate of the ket $\ket{\psi}$ in this paper - in the case of chiral and bichiral spinors, there are subtleties to be discussed.

The conjugates $\rhc{\psi}$ transform like
\begin{align}
	\rh{\psi} &\mapsto \grave{R} \rh{\psi} \\
	\rhc{\psi} &\mapsto \rhc{\psi} \grave{R}^\dag
\end{align}
under a rotation $R = \exp(B/2) = \exp(\theta_i \ I e_i/2)$. In the Pauli matrix representation of $\Cl(3)$, this rotor is
\begin{align}
	\grave{R} = \exp(\theta_i \ i P_i / 2), \label{eq:three-rotor pauli rep}
\end{align}
a special unitary matrix\footnote{In fact, the mapping of rotors to unitary two-by-two matrices in $\SU(2)$ is bijective.}. The generators $P_i / 2$ have eigenvalues $\pm 1/2$, which means that the spinors we just constructed do indeed have spin 1/2. This becomes particularly clear when we choose a spinor $\rh{\psi}$ such that the plane $B$ along which we rotate is an eigen-bivector to $\rh{\psi}$:
\begin{align}
	\frac{\grave{B}}{2} \rh{\psi} = \pm \frac{i}{2} \rh{\psi}
\end{align}
For instance, in the Pauli matrix representation, the generator $e_{12} = I e_3$ (rotation around the $z$ axis) has the matrix representation
\begin{align}
	\grave{e}_{12} = \frac{i}{2} \begin{pmatrix} 1 & 0 \\ 0 & -1 \end{pmatrix}
\end{align}
eigenspinors
\begin{align}
	\rh{+} &= \begin{pmatrix} 1 \\ 0 \end{pmatrix} \\
	\rh{-} &= \begin{pmatrix} 0 \\ 1 \end{pmatrix},
\end{align}
corresponding to the eigenvalues $+1/2$ and $-1/2$ respectively. By convention, they are referred to as \textit{spin-up} and \textit{spin-down}. The reader should \emph{not} think of them as ``arrows'' or ``vectors'' pointing along the $z$ axis - a vector parallel to the $z$ axis would stay the same under a rotation around the $z$ axis, but the spinor $\rh{+}$ does not.

The transformation law for e.g. $\rh{+}$ under rotation around the $z$ axis by an infinitesimal angle $\theta$ is
\begin{align}
	\rh{+_\theta} = \rh{+} + \frac{i}{2} \theta \rh{+}.
\end{align}
Integrating this yields
\begin{align}
	\rh{+_\theta} = \exp(\frac{i}{2} \theta) \rh{+}.
\end{align}
This means that the spinor $\rh{+}$ will not return to its original state after a $360^\circ$ rotation, but instead flip its sign:
\begin{align}
	\rh{+_{2\pi}} = \exp(i\pi) \rh{+} = - \rh{+}
\end{align}
Another rotation by $360^\circ$ restores the spinor to its original state. This angle doubling with respect to vectors is analogous to the angle doubling between vectors and ellipsoids.

In introductory quantum mechanics, it is customary to dismiss the complex phase introduced by rotating $\rh{+}$ around the $z$ axis as a ``meaningless global phase'' if only one spinor is involved. However, we argue that this phase does indeed have geometric significance.

\subsubsection{Conjugate spinors and down-projections}
Because $\grave{R}$ is a special unitary matrix, a conjugate spinor $\rhc{\psi}$ just transforms with the inverse of $\grave{R}$:
\begin{align}
	\rhc{\psi} &\mapsto \rhc{\psi} \Rh{R}^\dag \\ 
		   &= \rhc{\psi} \grave{R}^{-1} \\
		   &= \rhc{\psi} \grave{\Tilde{R}}
\end{align}
This means that we can construct a scalar from two space spinors $\rh{\psi}$, $\rh{\phi}$ as
\begin{align}
	\langle\grave{\psi} | \grave{\phi} \rangle \mapsto \rhc{\psi} \grave{\Tilde{R}} \grave{R} \rh{\phi} = \langle\grave{\psi} | \grave{\phi} \rangle.
\end{align}
Therefore, complex-conjugated space pinors $\rhc{\psi}$ are elements of the space \textit{cospinor} space $\grave{S}^{-1}$. 

Therefore, by taking the outer product between two spinors $\rh{\psi}$ and $\rh{\phi}$, we can construct a new two-by-two matrix $\rh{\psi} \rhc{\phi}$. Because the representation we are using is surjective, this new matrix also corresponds to a multivector. Like every other multivector, it transforms with a two-sided rotor transformation law:
\begin{align}
	\rh{\psi} \rhc{\phi} \mapsto \grave{R} \rh{\psi} \rhc{\phi} \grave{\Tilde{R}}.
\end{align}
This way of constructing multivectors out of spinors is analogous to the way we constructed quadratic forms out of vectors. We can even observe an analogous double cover phenomenon: The two rotors $R, -R \in \Spin(3)$ are linked by a $360^\circ$ rotation. They will correspond to different transformations of a spinor $\rhpsi \mapsto \pm \grave{R} \rhpsi$, but to the same multivector transform
\begin{align}
	M \mapsto (\pm R) M (\pm \Tilde{R}) = R M \Tilde{R}
\end{align}
We can thus identify $R$ and $-R$ with the same element of $\SO(3)$ - the rotor group $\Spin(3)$ is a double cover of $\SO(3)$. This means that we lose information when constructing the multivector $\rh{\psi} \rhc{\phi}$ out of two spinors $\rh{\psi}, \rh{\phi}$.

To illustrate, let us take a look at how the multivectors resulting from a down-projection of $\rh{+}$ and $\rh{\psi}$ look like respectively:
\begin{align}
	\rh{+} \rhc{+} &= \begin{pmatrix} 1 & 0 \\ 0 & 0 \end{pmatrix} = \frac{1}{2} (1 + \grave{e}_1) \\
	\rh{-} \rhc{-} &= \begin{pmatrix} 0 & 0 \\ 0 & 1 \end{pmatrix} = \frac{1}{2} (1 - \grave{e}_1)
\end{align}
The vector part of these down-projections is the classical ``spin vector'' associated with the spinor, i.e. the direction in which the angular momentum points:
\begin{align}
	S(\rh{\psi}) &= \left< \rh{\psi} \rhc{\psi} \right>_1 \\
	S\left( \grave{\ket{\begin{matrix} 1 \\ 0 \end{matrix}}} \right) &= \frac{1}{2} e_3 \\
	S\left( \grave{\ket{\begin{matrix} 0 \\ 1 \end{matrix}}} \right) &= -\frac{1}{2} e_3 \\
	S\left( \grave{\ket{\begin{matrix} 1 \\ 1 \end{matrix}}} \right) &= \frac{1}{2} e_1 \\
	\cdots
\end{align}

This relation is well-known from quantum mechanics as
\begin{align}
	\vec{S}(\psi) = \bra{\psi} \vec{\sigma} \ket{\psi}
\end{align}
which interprets the spin vector as the quantum-mechanical expectation value of the spin observable. In this paper, however, we will not use this viewpoint, and argue that spinors can be defined without reference to quantum mechanics. We will therefore not interpret such terms as ``observables'' and ``expectation values'', but as down-projections from spin-1/2 into spin-1 space, similar to how we down-projected vectors into spin-2 space to form ellipsoids. Furthermore, our full relativistic treatment of spinors will reveal that spin actually is a bivector or trivector depending on the context.

Now, we are equipped to try to answer the main question of the paper - what \textit{is} a spinor, geometrically? The answer is that spinors fundamentally are objects that lie outside of normal geometry. We cannot picture them, because the space we live in is spin-1 and not spin-1/2. What we \textit{can} do however is think about how scientists living in spin-2 space would think about spin-1 objects.

Imagine a 2D space whose coordinates are not vectors, but quadratic forms like in Figure \ref{fig:ellipse symmetry} - a sort of spin-2 equivalent of normal Minkowski, spin-1 space. Any field $\phi(\beta)$ defined on this space will be $180^\circ$-symmetric. Creatures living in such a spin-2 space would only develop geometric intuition for spin-2 objects. It would probably appear completely baffling to them how an object $v$ could \textit{not} be symmetric under a $180^\circ$ rotation, but instead flip its sign ($v \mapsto -v$), as they are only used to $180^\circ$-symmetric objects. There are several ways they could try to picture normal vectors - for instance, they could take a vector $v$ and form its down-projection $v v^T$ into spin-2 space. They could also define a ``reference vector'', calculate the matrix transforming that reference vector onto the vector they want to picture, decompose that matrix into an antisymmetric and symmetric part and map both to a quadratic form, flipping half of the signs of the antisymmetric part (with the obvious difference that these quadratic forms would transform very differently from normal spin-2 quadratic forms).

This is the analogy we are going to use to give readers a rough intution for spinors: A spinor $\rh{\psi}$ is a lower-spin object we inherently cannot picture in spin-1 space, but we can down-project it to a multivector $\rh{\psi} \rhc{\psi}$ out of it. For instance, the vector part of the spin-1 down-projection of $\rh{+}$ is the vector $e_3/2$ - but this does not mean that $\rh{\psi}$ \textit{is} a vector. This can be seen when considering a rotation around the $z$ axis - the vector $e_z$ is invariant under such a rotation, while the spinor $\rh{+}$ is not. Spinors fundamentally stand ``above'' the geometric algebra of space(time). In Figure \ref{fig:spinor analogy}, we lay out this analogy pictorially.

\begin{figure}
	\centering
	\begin{tikzpicture}
		\draw[->, thick] (0, 0) -- (10, 0) node[anchor=north west]{spin};
		\draw[thick] (1, 0) -- (1, -0.2) node[anchor=north]{$\frac{1}{2}$};
		\draw[thick] (5, 0) -- (5, -0.2) node[anchor=north]{$1$};
		\draw[thick] (9, 0) -- (9, -0.2) node[anchor=north]{$2$};
		\node at (1, 2.5) {\Huge ?};
		\draw[->, thick] (5, 2.2) -- (5.5, 3);
		\draw[rotate=58] (6.9, -6.1) ellipse (0.5 and 0.2);
		\node at (1, 3.8) {Spinors};
		\node at (5, 3.8) {Vectors};
		\node at (9, 3.8) {Ellipsoids};
		\draw[->, dashed] (1.45, 1.8) arc [start angle=-120,delta angle=60,radius=3] node[midway, anchor=north]{\small $\ket{\psi} \bra{\psi}$, take grade-1 part};
		\draw[->, dashed] (5.45, 1.8) arc [start angle=-120,delta angle=60,radius=3] node[midway, anchor=north] {\small $v v^T$, take symmetric part};


		\draw[->, thick] (0, 9) -- (10, 9) node[anchor=south west]{dimension};
		\draw[thick] (1, 9) -- (1, 9.2) node[anchor=south]{4};
		\draw[thick] (5, 9) -- (5, 9.2) node[anchor=south]{3};
		\draw[thick] (9, 9) -- (9, 9.2) node[anchor=south]{2};
		\node at (1, 6.5) {\Huge ?};
		\draw (4.5, 7) -- (5.5, 7) -- (5.5, 6) -- (4.5, 6) -- cycle;
		\draw (4.5, 7) -- (4.7, 7.2) -- (5.7, 7.2) -- (5.5, 7);
		\draw (5.7, 7.2) -- (5.7, 6.2) -- (5.5, 6);
		\draw[dashed] (4.5, 6) -- (4.7, 6.2) -- (4.7, 7.2);
		\draw[dashed] (4.7, 6.2) -- (5.7, 6.2);
		\draw (8.5, 7) -- (9.5, 7) -- (9.5, 6) -- (8.5, 6) -- cycle;
		\node at (1, 5.4) {4D objects};
		\node at (5, 5.4) {3D objects};
		\node at (9, 5.4) {2D objects};
		\draw[dotted, thick] (0, 4.6) -- +(10, 0);
		\draw[->, dashed] (1.45, 7.2) arc [start angle=120,delta angle=-60,radius=3] node[midway, anchor=south] {\small projection};
		\draw[->, dashed] (5.95, 7.2) arc [start angle=120,delta angle=-60,radius=3] node[midway, anchor=south] {\small projection};
	\end{tikzpicture}
	\caption{The relationship between spin-1/2, spin-1, and spin-2 objects is similar to that between 4D, 3D and 2D objects.}
	\label{fig:spinor analogy}
\end{figure}
