\subsection{Spin}
Noether's theorem relates continuous symmetries of a system to conserved quantities. In particular, the field-theoretic version of Noether's theorem states that every continuous symmetry of a field theorem corresponds to a conserved current $j^\mu$ with $\partial_\mu j^\mu = 0$. Given a field $\phi$ with a symmetry
\begin{align}
	\phi \mapsto \phi + \epsilon \delta \phi
\end{align}
resulting in a constant action density up to a divergence
\begin{align}
	\mathcal{L} \mapsto \mathcal{L} + \epsilon \partial_\mu W^\mu,
\end{align}
the associated conserved current is given by
\begin{align}
	j^\mu = \pdv{\mathcal{L}}{(\partial_\mu \phi)} * \delta \phi - W^\mu.
\end{align}
The component-wise product $*$ means that every degree of freedom of the theory that transforms under the symmetry corresponds to a part of the conserved current. For instance, the $\U(1)$ current of scalar electromagnetism ($\phi \mapsto \phi + i \epsilon \phi$) is
\begin{align}
	j^\mu = \pdv{\mathcal{L}}{(\partial_\mu \phi)} i \phi,
\end{align}
where $i$ is the complex imaginary unit. It is crucial to note that the exact representation of the symmetry group under which the field transforms has a bearing on the Noether current: If we were to introduce a second field $\phi'$ transforming as $\phi' \mapsto \phi' + 2 i \epsilon \phi'$, the Noether current would be
\begin{align}
	j^\mu = \pdv{\mathcal{L}}{(\partial_\mu \phi)} i\phi + 2\pdv{\mathcal{L}}{(\partial_\mu \phi')} i \phi'.
\end{align}
Roughly speaking, the faster a field transforms under a symmetry, the greater its contribution to the Noether current.

We now turn to rotational symmetry. When we apply an infinitesimal rotation parametrized by a bivector $B$ to a field theory, two things happen: First, the fields at position $x$ will be displaced by an amount $\epsilon \frac{B}{2} \times x$, where $\times$ denotes the commutator product. Second, if the field is not a scalar field, the values of the field itself will be transformed - for instance, if we rotate a vector field $A(x)$:
\begin{align}
	A(x) \mapsto A(x) + \underbrace{\epsilon \left( \frac{B}{2} \times x \right) \cdot \nabla A(x)}_{\text{field displacement}} \ \ \ + \underbrace{\epsilon \frac{B}{2} \times A(x)}_{\text{value transformation}}
\end{align}
The resulting conserved quantity is the angular momentum of the field. The field displacement part corresponds to the orbital angular momentum, while the value transformation part corresponds to the intrinsic angular momentum, or spin.

The spin current $S^\mu(B)$ of a field corresponding to the rotation of the field along $B$ is given by\footnote{Note that this is not a conserved current by itself - we have left out the orbital angular momentum part for clarity.}
\begin{align}
	S^\mu(B) = \pdv{\mathcal{L}}{(\partial_\mu A)} * \left( \frac{B}{2} \times A \right).
\end{align}
The linear map $x \mapsto \frac{B}{2} \times x$ is a generator of the $SO(3)$ representation of the Lie group of rotors. Written out in matrix form, these generators are spanned by the basis matrices
\begin{align}
	T_1 = \begin{pmatrix} 0 & 0 & 0 \\ 0 & 0 & 1 \\ 0 & -1 & 0 \end{pmatrix} \ \ \ T_2 = \begin{pmatrix} 0 & 0 & -1 \\ 0 & 0 & 0 \\ 1 & 0 & 0 \end{pmatrix} \ \ \ T_3 = \begin{pmatrix} 0 & 1 & 0 \\ -1 & 0 & 0 \\ 0 & 0 & 0 \end{pmatrix},
\end{align}
corresponding to the bivectors $\gamma^2 \wedge \gamma^3$, $\gamma^3 \wedge \gamma^1$ and $\gamma^1 \wedge \gamma^2$ respectively. These matrices\footnote{We have used the physics convention for Lie algebras in which generators are defined with an additional factor of $i$. If we were to use the mathematics convention, the eigenvalues would be $\pm i, 0$.} have eigenvalues $-1, 0, +1$. As the spin current is proportional to $T_i A(x)$, the field can therefore have spin values of $0$ and $ \pm1$. These correspond to longitudinal and circular polarization respectively, as can be seen from the eigenvectors of e.g. $T_3$ corresponding to these eigenvalues:
\begin{align}
	v_+ &= \begin{pmatrix} 1 \\ i \\ 0 \end{pmatrix} \ \
	v_- = \begin{pmatrix} 1 \\ -i \\ 0 \end{pmatrix} \ \
	v_0 = \begin{pmatrix} 0 \\ 0 \\ 1 \end{pmatrix},
\end{align}
such that
\begin{align}
	T_3 v_+ &= \begin{pmatrix} i \\ -1 \\ 0 \end{pmatrix} = +i v_+ \\
	T_3 v_- &= \begin{pmatrix} -i \\ -1 \\ 0 \end{pmatrix} = -i v_- \\
	T_3 v_0 &= 0.
\end{align}
This is the central point that links representation theory to spin: The eigenvalues of the generators determine the possible values the spin of the field can take. To further illustrate this, we will now take a look at how quadradic forms transform using the matrix-vector formalism.
