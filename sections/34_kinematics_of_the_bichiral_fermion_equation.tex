\subsection{Kinematics of the bichiral fermion equation}
The bichiral fermion equation
\begin{align}
	(i \partial - m) \ket{\psi} = 0
\end{align}
implies the Klein-Gordon equation
\begin{align}
	(i \partial + m) (i \partial - m) \psi = (\partial^2 + m^2) \psi = 0.
\end{align}
Thus, the bichiral fermion field obeys the massive dispersion relation $p^2 = m^2$. We can thus interpret the coupling strength between the left-chiral and right-chiral fermion as the mass of the resulting bichiral fermion.

We guess a plane-wave solution of the form
\begin{align}
	\psi(x) = \ket{u} e^{-ipx},
\end{align}
where $\ket{u}$ is some bichiral spinor to be determined, and $p$ should fulfill the massive dispersion relation $p^2 = m^2$. Inserting this into the bichiral equation yields
\begin{align}
	(p - m) \ket{u} = 0, \label{eq:3_dirac_k}
\end{align}
the momentum-space bichiral fermion equation. To solve it, we briefly descend from our ivory tower and explicitly write down (\ref{eq:3_dirac_k}) in matrix form for the special case $p = m \gamma_0$:
\begin{align}
	m(\gamma^0 - 1) \ket{u} &= 0 \\
	= m \begin{pmatrix} -1 & 1 \\ 1 & -1 \end{pmatrix} \begin{pmatrix} \lh{u_{\text{LH}}} \\ \rh{u_{\text{RH}}} \end{pmatrix} &= 0
\end{align}
This is solved by
\begin{align}
	\ket{u_0} = \begin{pmatrix} \lh{u_{\text{LH}}} \\ \rh{u_{\text{RH}}} \end{pmatrix} = \begin{pmatrix} \xi_0 \\ \xi_0 \end{pmatrix},
\end{align}
where $\xi_0 \in \BC^2$. We set
\begin{align}
	\xi_0 = \sqrt{2} \begin{pmatrix} 1 \\ 0 \end{pmatrix}
\end{align}
The corresponding down-projected multivector $\ket{u_0} \bra{u_0}$ is:
\begin{align}
	\ket{u_0} \bra{u_0} &= \sqrt{2}^2 \begin{pmatrix} 1 & 0 & 1 & 0 \\ 0 & 0 & 0 & 0 \\ 1 & 0 & 1 & 0 \\ 0 & 0 & 0 & 0 \end{pmatrix} \\
			&= 1 + \gamma_0 + i\gamma_{12} + i \gamma_{012} \\
			&= (1 + \gamma_0)(1 + i\gamma_{12})
\end{align}
The $i$ in this equation is the complex imaginary unit, and not the pseudoscalar.

In order to obtain the solution for a momentum $p$ different from $p_0 = m \gamma_0$, we simply boost the solution we just derived. As this solution $\ket{u_0}$ is valid for $m\gamma_0$, we need to boost it with a rotor
\begin{align}
	R_{p} = \sqrt{\frac{p \gamma_0}{m}} = \sqrt{U \gamma_0},
\end{align}
where $U p/m$ is the four-velocity of the particle. Note that the rotor $R_p = U \gamma_0$ would have effected twice the boost we want. This yields a generalized solution of:
\begin{align}
	\ket{u(p)} = R_p \ket{u_0} = \sqrt{U \gamma_0} \ket{u_0} = \begin{pmatrix} \Lh{\sqrt{U \gamma_0}} \ \xi \\ \Rh{\sqrt{U \gamma_0}} \ \xi \end{pmatrix}
\end{align}

Furthermore, we can choose any $\xi$ instead of just $\xi_0 = \begin{pmatrix} 1 & 0 \end{pmatrix}^T$. For any $\xi, \xi_0$, we can find a spatial rotor that relates them with each other:
\begin{align}
	\xi = \grave{R}_\xi \xi_0
\end{align}
Therefore, we can write down the following general solution to the bichiral equation:
\begin{align}
	\ket{u(p, \xi)} = R_p R_\xi \ket{u_0}
\end{align}
This down-projects to
\begin{align}
	\ket{u(p, \xi)} \bra{u(p, \xi)} &= R_p R_\xi \ket{u_0} \bra{u_0} \Tilde{R}_\xi \Tilde{R}_p \\
					&= (1 + U)(1 + is),
\end{align}
where $s$ is the spin bivector of the particle specified by $\xi$. It is orthogonal to $U$. Later, we will see that all of these grades have a physical meaning - up to a constant factor, the scalar grade corresponds to the charge of the field, the vector grade to the four-velocity of the particle, the bivector grade to the spin charge and the trivector grade to the spin current of the particle.

It should be noted that it is also common practice to norm bichiral spinors with a factor of $\sqrt{2m}$ instead of $\sqrt{2}$. This results in a down-projection of
\begin{align}
	\ket{u} \bra{u} = (m + p)(1 + is). \label{eq:3_bichiral_spinor_dp}.
\end{align}
In this form, it is particularly easy to see why such spinors are solutions of the bichiral equation: Postmultiplying the bichiral equation in momentum space with $\bra{u}$ yields:
\begin{align}
	(p - m) \ket{u} \bra{u} = 0.
\end{align}
When we insert (\ref{eq:3_bichiral_spinor_dp}), we see that:
\begin{align}
	(p - m)(p + m) (1 + is) = (p^2 - m^2)(1 + is) = 0,
\end{align}
as $p^2 = m^2$. This implies that $(p - m) \ket{u} = 0$, too.

\subsubsection{Antiparticles}
The avid reader will have noticed that we have not yet covered all possible solutions - so far, we have taken a reference momentum $p_0 = m \gamma_0$ and boosted it with some arbitrary rotor. This will only result in positive frequencies $p_0 > 0$. There is, however, another class of solutions with negative frequencies. 

For every positive-frequency $p, p_0 > 0$, there is a corresponding negative-frequency $p' = -p$. Let $\ket{u(p)}$ be a solution of the bichiral equation at $p$,
\begin{align}
	(p - m)\ket{u(p)} = 0.
\end{align}
Then, using the \textit{charge conjugation operator}
\begin{align}
	\ket{\psi} \mapsto C \ket{\psi}^*,
\end{align}
where $C$ is a four-by-four matrix:
\begin{align}
	C = \begin{pmatrix} 0 & \epsilon \\ -\epsilon & 0 \end{pmatrix}.
\end{align}
Then, the spinor $\ket{v(p)} := C \ket{u(p)}^*$ is a solution of the bichiral equation at $-p$,
\begin{align}
	(-p - m)\ket{v(p)} = 0.
\end{align}
This is easy to prove: Postmultiplying this equation with $\bra{v(p)}$ yields:
\begin{align}
	(-p - m)\ket{v(p)} \bra{v(p)} = 0.
\end{align}
This equation is equivalent to the previous one. We now calculate the down-projection $\ket{v(p)} \bra{v(p)}$:
\begin{align}
	\ket{v(p)} \bra{v(p)} = \begin{pmatrix} - \epsilon (\lh{u_L} \lhc{u_R})^* \epsilon^T & \epsilon (\lh{u_L} \rhc{u_L})^* \epsilon^T \\ \epsilon (\rh{u_R} \lhc{u_R})^* \epsilon^T & - \epsilon (\rh{u_R} \rhc{u_L})^* \epsilon^T \end{pmatrix}
\end{align}
We can see that the even part of the original down-projection $\ket{u(p)}$ is multiplied by $(-1)$, while the odd part stays the same. Additionally, the imaginary part flips sign because of the complex conjugations. Therefore,
\begin{align}
	\ket{v(p)} \bra{v(p)} = (-1 + U)(1 - is) = \frac{1}{m} (-m + p)(1 -is), \label{eq:3_bichiral_anti_dp}
\end{align}
such that
\begin{align}
	(-p - m) \ket{v(p)}\bra{v(p)} = -\frac{1}{m} (p + m)(p - m) (1 - is) = (p^2 - m^2)(1 - is) = 0.
\end{align}
Explicitly, the solution $\ket{v(p, \xi)}$ is given by
\begin{align}
	\ket{v(p, \xi)} = \begin{pmatrix} \Lh{\sqrt{U \gamma_0}} \epsilon \xi^* \\ - \Rh{\sqrt{U \gamma_0}} \epsilon \xi^* \end{pmatrix}.
\end{align}

\subsubsection{Spin sum relations}
When calculating unpolarized scattering amplitudes in quantum field theory, we are interested in the down-projection of a bichiral spinor averaged over all possible spin states:
\begin{align}
	\frac{1}{2} \sum_{\xi \in \{+, -\}} \ket{u(p, \xi)} \bra{u(p, \xi)} = (+1 + U) \\
	\frac{1}{2} \sum_{\xi \in \{+, -\}} \ket{v(p, \xi)} \bra{v(p, \xi)} = (-1 + U).
\end{align}
In the standard formalism, these relations are tedious to obtain. In our formalism, however, they are a trivial corollary to (\ref{eq:3_bichiral_spinor_dp}) and (\ref{eq:3_bichiral_anti_dp}).


\subsubsection{Chiral projectors}
The concept of bichiral fermions arose because we wanted to make computations including coupled pairs of right-chiral and left-chiral fermions more compact. We found that such a coupled pair of particles behaves like a single particle, a bichiral fermion. However, it can still be necessary to ``decompose'' a bichiral fermion into its constituent chiral fermions - most prominently in the theory of weak interaction, where only the left-chiral fermions carry weak isospin charge. At first sight, it appears that we have to abandon our formalism of bichiral fermions in such cases and return to calculations in terms of chiral fermions. However, this can be avoided with the introduction of \textit{chiral projectors}.

We consider the multivector $-iI$, i.e. the STA pseudoscalar multiplied by $-1$ times the complex imaginary unit. The matrix representation of this multivector is
\begin{align}
	-iI = \begin{pmatrix} -1_{2\times 2} & 0 \\ 0 & 1_{2\times 2} \end{pmatrix}.
\end{align}
Now, let
\begin{align}
	\ket{\psi_R} &= \begin{pmatrix} 0 \\ \rh{\psi_R} \end{pmatrix} \\
	\ket{\psi_L} &= \begin{pmatrix} \lh{\psi_L} \\ 0 \end{pmatrix}
\end{align}
be a purely right-handed and left-handed bichiral spinor respectively. The eigenvalues of these spinors with respect to $-iI$ are:
\begin{align}
	-iI \ket{\psi_R} &= \ket{\psi_R} \ \ \Rightarrow \ \text{eigenvalue +1} \\
	-iI \ket{\psi_L} &= -\ket{\psi_L} \ \ \Rightarrow \ \text{eigenvalue -1}.
\end{align}
For this reason, we will call $-iI$ the \textit{chiral element}. We can ``project out'' the right-chiral and left-chiral parts of an arbitrary bichiral spinor $\ket{\psi} = \begin{pmatrix} \lh{\psi_L} \\ \rh{\psi_R} \end{pmatrix}$ with the \textit{chiral projectors}
\begin{align}
	P_R &= \frac{1 - iI}{2} = \begin{pmatrix} 0 & 0 \\ 0 & 1_{2 \times 2} \end{pmatrix} \\
	P_L &= \frac{1 + iI}{2} = \begin{pmatrix} 1_{2 \times 2} & 0 \\ 0 & 0 \end{pmatrix},
\end{align}
such that:
\begin{align}
	P_R \ket{\psi} &= \frac{1 - iI}{2} \ket{\psi} = \begin{pmatrix} 0 \\ \rh{\psi_R} \end{pmatrix} \\
	P_L \ket{\psi} &= \frac{1 + iI}{2} \ket{\psi} = \begin{pmatrix} \lh{\psi_L} \\ 0 \end{pmatrix}.
\end{align}
For instance, the matrix amplitude of the interaction of a $W$ boson with a fermion $\ket{\psi}$ only depends on the left-handed component. Thus, whenever the bichiral spinor $\ket{\psi}$ appears in such an amplitude, it would be in terms of $P_L \ket{\psi}$.

\iffalse
In principle, we can also extract the RH/LH component of a bichiral spinor by boosting it parallel/antiparallel to its spin pseudovector. For instance, if we start with a spin-up bichiral fermion at rest,
\begin{align}
	\ket{\psi} = \begin{pmatrix} 1 \\ 0 \\ 1 \\ 0 \end{pmatrix},
\end{align}
and perform a photoid boost in the $+z$ direction:\footnote{We are inserting a normalization factor of $e^{-\eta/2}$ for convenience.}
\begin{align}
	R &= \lim_{\eta \to \infty} \exp(\gamma_{30} \ \eta /2) \propto \frac{1 + \gamma_{30}}{2} \\
	R \ket{\psi} &\propto \begin{pmatrix} 0 & 0 & & \\ 0 & 1 & & \\ & & 1 & 0 \\ & & 0 & 0 \end{pmatrix} \begin{pmatrix} 1 \\ 0 \\ 1 \\ 0 \end{pmatrix} = \begin{pmatrix} 0 \\ 0 \\ 1 \\ 0 \end{pmatrix},
\end{align}
such that only the right-handed component remains. However, we cannot define a projector like this, as we always need to perform the boost in the direction of the spin pseudovector and thus would need to choose different bivectors instead of $\gamma_{30}$ depending on the spinor. Looking back at the chiral projectors $P_{R/L}$, however, we realize that this is exactly what they do: Applying a chiral projector to a spinor yields the same result as we would have obtained by performing a photoid boost along or against its spin bivector. Therefore, we can say that the chiral element $-iI$ acts like a generator of a boost in the direction of the spin pseudovector.
\fi

\subsubsection{On-shell projectors}
Consider an arbitary bichiral spinor $\ket{\psi}$ that is not necessarily a solution for the bichiral equation for a certain momentum, 
\begin{align}
	(p - m) \ket{\psi} \stackrel{?}{=} 0.
\end{align}
However, we can with certainty say that the spinor $\ket{\psi'} = (p + m) \ket{\psi}$ is a solution to the above equation:
\begin{align}
	(p - m) \ket{\psi'} = (p - m)(p + m) \ket{\psi} = (p^2 - m^2) \ket{\psi} = 0.
\end{align}
If $\ket{\psi}$ already is a solution,
\begin{align}
	(p + m)\ket{\psi} = 2m \ket{\psi}.
\end{align}
This can be shown by postmultiplying with $\bra{\psi}$:
\begin{align}
	&(p + m) \ket{\psi} \bra{\psi} \\
	&= (p + m) (1 + U) ( 1 + is) \\
	&= m (1 + U)^2 (1 + is) \\
	&= 2m (1 + U) (1 + is) \\
	&= 2m \ket{\psi} \bra{\psi} \\
	&\Rightarrow (p + m) \psi = 2m \ket{\psi}.
\end{align}
Therefore, it seems logical to define the \textit{on-shell projector} as the linear operator
\begin{align}
	P_p = \frac{1}{2m} (p + m) = \frac{1 + U}{2}.
\end{align}
This projector can be interpreted as ``projecting out'' the part of an arbitrary bichiral spinor that is a solution to the bichiral equation at the momentum mode $p$:
\begin{align}
	(p - m) P_p \ket{\psi} = 0 \ \ \text{for all $\ket{\psi}$}.
\end{align}
If $\ket{\psi}$ already is a solution, then $P_p \ket{\psi} = \ket{\psi}$. Note the resemblance between this on-shell projector and the chiral projectors - for a solution $\ket{u(p)}$, we have $U \ket{u(p)} = \ket{u(p)}$. On the other hand, for the corresponding antiparticle solution $\ket{v(p)}$, we have $U \ket{v(p)} = - \ket{v(p)}$. Viewed like this, an antiparticle solution $\ket{v(p)}$ does maximally not solve the bichiral equation at $p$.

This projector also appears in the Dirac propagator
\begin{align}
	D_F(p) = \frac{i}{p - m} = \frac{i(p + m)}{p^2 - m^2} = \frac{2im P_p}{p^2 - m^2}. \label{eq:3_dirac_propagator}
\end{align}
Roughly speaking, the Dirac propagator describes how a bichiral spinor field reacts when coupled to a source. In principle, this source can be any arbitary bichiral spinor field and does not need to fulfill any equations of motion. The denominator of (\ref{eq:3_dirac_propagator}) filters out the parts of the external source that do not fulfill the Klein-Gordon on-shell condition $p^2 = m^2$, while the numerator filters out the parts of the spinor that do not fulfill the spinorial on-shell condition $(p - m) \ket{\psi} = 0$. Thus, only the parts that actually contribute to the excitation of the fermion field remain.
