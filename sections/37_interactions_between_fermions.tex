\subsection{Interactions between fermions}
Now that we have derived how the observables of particles and antiparticles behave, we are ready to examine the possible types of interactions between bichiral fermions.

The most well-known type of interaction is the interaction via a gauge boson vector field $A$. It is commonly formulated by introducing a covariant derivative
\begin{align}
	D = \partial - iqA
\end{align}
and replacing the partial vector derivative in the bichiral fermion action with the covariant vector derivative:
\begin{align}
	\mathcal{L} &= \expval{(iD - m) \ket{\psi} \bra{\psi}} \\
		    &= \expval{(i\partial -m ) \ket{\psi} \bra{\psi}} + q A \cdot \expval{\ket{\psi} \bra{\psi}}_1
\end{align}
We then split off the interaction Lagrangian $\mathcal{L}_{\text{int}} = qA \cdot \expval{\ket{\psi} \bra{\psi}}_1$ and treat it perturbatively. The classical electromagnetic potential interaction between two particles is then described by the $t$-channel scattering of two fermions.

\begin{figure}
	\centering
	\begin{tikzpicture}
		\draw (-2, 0) node [anchor=north] {$\ket{\psi_a}$} -- (-2, 2) node [anchor=south] {$\bra{\psi_a}$};
		\draw[-{Latex}] (-2, 0) -- (-2, 0.575);
		\draw[-{Latex}] (-2, 1) -- (-2, 1.575);
		\draw[-{Latex}] (2, 0) -- (2, 0.575);
		\draw[-{Latex}] (2, 1) -- (2, 1.575);
		\draw (2, 0) node [anchor=north] {$\ket{\psi_b}$} -- (2, 2) node [anchor=south] {$\bra{\psi_b}$};
		\fill (-2, 1) circle(1.5pt);
		\fill (2, 1) circle(1.5pt);
		\draw[decorate, decoration=snake] (-2, 1) -- (2, 1);
	\end{tikzpicture}
	\label{fig:3_particle_particle_interaction}
	\caption{Interaction between two particles.}
\end{figure}
\begin{figure}
	\centering
	\begin{tikzpicture}
		\draw (-2, 0) node [anchor=north] {$\ket{\psi_a}$} -- (-2, 2) node [anchor=south] {$\bra{\psi_a}$};
		\draw[-{Latex}] (-2, 0) -- (-2, 0.575);
		\draw[-{Latex}] (-2, 1) -- (-2, 1.575);
		\draw[-{Latex}] (2, 1) -- (2, 0.325);
		\draw[-{Latex}] (2, 2) -- (2, 1.325);
		\draw (2, 0) node [anchor=north] {$\bra{\psi_b}$} -- (2, 2) node [anchor=south] {$\ket{\psi_b}$};
		\fill (-2, 1) circle(1.5pt);
		\fill (2, 1) circle(1.5pt);
		\draw[decorate, decoration=snake] (-2, 1) -- (2, 1);
	\end{tikzpicture}
	\label{fig:3_particle_antiparticle_interaction}
	\caption{Interaction between a particle and an antiparticle.}
\end{figure}

The propagator of the electromagnetic field is given by
\begin{align}
	D_{\mu \nu}(k) = \frac{-i g_{\mu \nu}}{k^2 + i0^+},
\end{align}
such that the matrix amplitude for a t-channel scattering is given by
\begin{align}
	\mathcal{M}_{\textit{fi}} &= j_1^\mu D_{\mu \nu}(k) j_2^\nu \\
				  &= -i \frac{j_1 \cdot j_2}{k^2 + i0^+} \\
				  &= -i \frac{\expval{\ket{\psi_a} \bra{\psi_a}}_1 \cdot \expval{\ket{\psi_b} \bra{\psi_b}}_1}{t + i0^+}
\end{align}

We assume that the two particles are tightly localized around two positions $\mathbf{r}_1, \mathbf{r}_2$ respectively. Fourier-transforming the propagator then allows us to rewrite this scattering amplitude as a potential:
\begin{align}
	V(\mathbf{r}_1, \mathbf{r}_2) = \frac{\expval{\ket{\psi_a} \bra{\psi_a}}_1 \cdot \expval{\ket{\psi_b} \bra{\psi_b}}_1}{|\mathbf{r}_1 - \mathbf{r}_2|} = q^2 \frac{U_a \cdot U_b}{|\mathbf{r}_1 - \mathbf{r}_2|}.
\end{align}
where we have neglected the bound currents that would result in a spin-spin interaction.
% TODO: Maybe actually do the Fourier transform and make this a bit less sloppy.
This potential is repulsive if $U_a, U_b$ point into the same timelike half of the light coned - i.e., if both of them are particles or both of them are antiparticles. If one is a particle and the other is an antiparticle, however, the numerator will be negative and the potential will be attractive. This matches our expectations from classical electrodynamics.


\subsubsection{Interactions of other grades}
As we have seen, electromagnetism is an interaction coupling to the vector part of the down-projections of the fermions, i.e. to the conserved $\U(1)$ Noether current. However, interactions of grades other than vector-grade are very much possible. For instance, the \textit{Yukawa interaction} couples a scalar boson field $\phi$ to the scalar observable of a fermionic field:
\begin{align}
	\mathcal{L}_{\text{Yukawa}} = -\lambda \expval{ \phi \ket{\psi} \bra{\psi}}_0
\end{align}
The propagator for a scalar boson field is given by
\begin{align}
	D(k) = \frac{i}{k^2 - M^2 + i0^+},
\end{align}
where $M$ is the mass of the scalar boson field. Therefore, the t-channel scattering amplitude via a Yukawa interaction is given by:
\begin{align}
	\mathcal{M}_{\text{fi}} &= i \lambda^2 \frac{\expval{\ket{\psi_a} \bra{\psi_a}}_0 \expval{\ket{\psi_a} \bra{\psi}}_0}{k^2 - M^2 + i0^+}.
\end{align}
Fourier-transforming this yields
\begin{align}
	V(\mathbf{r}_1, \mathbf{r}_2) = - \lambda^2 \frac{\expval{\ket{\psi_a} \bra{\psi_a}}_0 \expval{\ket{\psi_b} \bra{\psi_b}}_0}{|\mathbf{r}_1 - \mathbf{r}_2|} e^{-iM|\mathbf{r}_1 - \mathbf{r}_2|}.
\end{align}
The scalar part of $\ket{\psi} \bra{\psi}$ is equal to one for both well-normed particle and antiparticle states. This means that
\begin{align}
	V(\mathbf{r}_1, \mathbf{r}_2) = - \lambda^2 \frac{1}{|\mathbf{r}_1 - \mathbf{r}_2|} e^{-iM|\mathbf{r}_1 - \mathbf{r}_2|}.
\end{align}
We see that the Yukawa interaction is always attractive. Similarly, we can e.g. see that the magnetic spin-spin coupling for particle-particle or antiparticle-antiparticle interactions is attractive for antiparallel spins, and repulsive for parallel spins. For particle-antiparticle interactions, it is repulsive for antiparallel spins and attractive for parallel spins.
