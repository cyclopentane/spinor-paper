\subsection{Going relativistic: Chiral spinors}

We will now go on to extend our 3D theory of spinors to 3+1 dimensional Minkowski space in order to find the relativistic equivalent, i.e. \textit{spacetime spinors}. To do so, we have to find a representation for the relativistic rotor group $\Spin^+(1,3)$. The most obvious way to do this would be to find a representation of the full STA $\Cl(1, 3)$ and take the representation of the $\Spin(1, 3)$ subalgebra from it. However, this is not necessary: $\Spin^+(1, 3)$ lies in the even subalgebra $\Cl^+(1, 3)$, which is isomorphic to $\Cl(3)$ via the space-time observer split. We therefore single out a timelike vector (conventionally taken to be $\gamma_0$), and define the basis of the even subalgebra to be $\sigma_\mu = \gamma_\mu \gamma_0$. We now define the \textit{right-chiral representation}
\begin{align}
	\grave{\sigma}_\mu = P_\mu,
\end{align}
with $P_\mu = (1, P_i)$. The analogous \textit{left-chiral representation} is
\begin{align}
	\acute{\sigma}_\mu = \bar{P}_\mu
\end{align}
with $\bar{P}_\mu = (1, -P_i)$, the parity-flipped Pauli matrices.\footnote{The conventional notation is $\sigma_\mu = (1, \sigma_i)$ and $\bar{\sigma}_\mu = (1, -\sigma_i)$.} We will call spinors transforming under these representations \textit{chiral spinors}.\footnote{Conventionally known as ``Weyl spinors'' or ``chiral eigenstates of the Dirac equation''.} We will write $\rh{\psi} \in \Rh{W}$ for right-chiral spinors, and $\lh{\psi} \in \Lh{W}$ for left-chiral spinors.

A general spacetime rotor is given by
\begin{align}
	R = \exp(\frac{B}{2}),
\end{align}
where $B$ is the bivector generating the transformation:
\begin{align}
	B = I \sigma_i \theta_i + \sigma_i \eta_i.
\end{align}
The parameters $\theta_i$ and $\eta_i$ correspond to rotation angles and boost rapidities respectively. With this decomposition, the right-chiral representation of $R$ is
\begin{align}
	\grave{R} &= \exp(\frac{\grave{I} \grave{\sigma}_i}{2} \theta_i + \frac{\grave{\sigma}_i}{2} \eta_i) \\
		  &= \exp(\frac{i P_i}{2} \theta_i + \frac{P_i}{2} \eta_i).
\end{align}
Analogously, the left-chiral representation of $R$ is
\begin{align}
	\acute{R} &= \exp(\frac{\acute{I} \acute{\sigma}_i}{2} \theta_i + \frac{\acute{\sigma}_i}{2} \eta_i) \\
		  &= \exp(\frac{i P_i}{2} \theta_i - \frac{P_i}{2} \eta_i).
\end{align}
Both reduce to the Pauli matrix representation (\ref{eq:three-rotor pauli rep}) when no boosts are involved. However, once $\eta_i \neq 0$, the matrices leave the $\SU(2)$ group; the chiral representation of spacetime rotors is $\SL(2, \BC)$.

The importance of the distinction between left-handed and right-handed representations becomes apparent: The sign in front of the respective boost generators are flipped, such that once $\eta_i \neq 0$, the left-chiral and right-chiral representations of $R$ are not equal to each other anymore.

Thus, for pure rotations, right-handed and left-handed chiral spinors transform the same way, but under boosts, they transform opposite to each other. For instance, right-hand spin-up
\begin{align}
	\rh{+} = \begin{pmatrix} 1 \\ 0 \end{pmatrix}
\end{align}
will grow exponentially under a $+z$ boost, while left-hand spin-down
\begin{align}
	\lh{-} = \begin{pmatrix} 1 \\ 0 \end{pmatrix}
\end{align}
will shrink exponentially.

Thus, the hermitean conjugate of a right-chiral (left-chiral) spinor $\rh{\psi}$ does \textit{not} transform like an element of the dual right-chiral (left-chiral) spinor space anymore:
\begin{align}
	\rh{\psi}^\dag \mapsto \rh{\psi}^\dag \grave{R}^\dag &= \rh{\psi}^\dag \exp(-\frac{iP_i}{2} \theta_i + \frac{P_i}{2} \eta_i) = \rh{\psi}^\dag \acute{R}^{-1}
\end{align}
Instead, the hermitean conjugate of the right-chiral (left-chiral) spinor $\rh{\psi}$ transforms like an element of the dual space of the \textit{left-chiral} (right-chiral) spinors, and vice versa. For chiral spinors, we will thus write:
\begin{align}
	\lhc{\psi} &:= \rh{\psi}^\dag \\
	\rhc{\phi} &:= \lh{\phi}^\dag
\end{align}
This is a very important point, so let us stress it: Given two chiral spinors $\rh{\psi_R}, \lh{\psi_L}$, the products
\begin{align}
	&\langle \grave{\psi_L} | \grave{\psi_R} \rangle \\
	&\langle \acute{\psi_R} | \acute{\psi_L} \rangle
\end{align}
are invariants, but
\begin{align}
	&\langle \acute{\psi_R} | \grave{\psi_R} \rangle \\
	&\langle \grave{\psi_L} | \acute{\psi_L} \rangle
\end{align}
are not.

\subsubsection{Even down-projections}
In analogy to how we down-projected two Pauli spinors, we might take two right-chiral (left-chiral) spinors $\rh{\psi}, \rh{\phi}$ by forming the outer product between them:
\begin{align}
	\rh{\psi} \lhc{\phi} \label{eq:2_naïve_weyl_projection}
\end{align}
While we did not need to pay attention to chirality in the case of space spinors, we absolutely have to do so in the case of chiral spinors. To see why, let us postmultiply a left-chiral spinor $\lh{\xi}$ to (\ref{eq:2_naïve_weyl_projection}):
\begin{align}
	\rh{\psi} \langle \acute{\phi} | \acute{\xi} \rangle
\end{align}
The last part is just an invariant scalar, so the result is a right-chiral spinor. We see that something is wrong: As the representation of a multivector is an \textit{endo}morphism, we would expect that it maps left-chiral onto left-chiral (right-chiral onto right-chiral) spinors. 

Thus, we need to combine a right-chiral and a left-chiral spinor $\rh{\psi}$, $\lh{\xi}$ to form an even multivector:
\begin{align}
	\grave{M} = \rh{\psi} \rhc{\xi} \\
	\acute{N} = \lh{\xi} \lhc{\psi},
\end{align}
which will then map right-chiral onto right-chiral (left-chiral onto left-chiral) spinors, and transform like we expect it from a representation of a multivector:
\begin{align}
	\grave{M} &\mapsto \grave{R} \grave{M} \grave{R}^{-1} \\
	\acute{M} &\mapsto \acute{R} \acute{M} \acute{R}^{-1}
\end{align}
As the chiral representation covers the even subalgebra $\Cl^+(1, 3)$, $M$ and $N$ are even multivectors. For this reason, we call a down-projection of a left-handed and a right-chiral spinor $\rh{\psi}$, $\lh{\xi}$ an \textit{even} down-projection. An even down-rojection always yields an even multivector.

\subsubsection{Odd down-projections}
In contrast, we will call a down-projection of two chiral spinors of equal chirality an \textit{odd} down-projection and denote its results with hats and checks:
\begin{align}
	\hat{A} &= \lh{\zeta} \rhc{\xi} \\
	\check{B} &= \rh{\psi} \lhc{\phi}
\end{align}

The odd matrices map right-chiral onto left-chiral spinors, and vice versa:\footnote{Hence the choice of notation.}
\begin{align}
	\lh{\psi'} &= \hat{A} \rh{\psi} \\
	\rh{\xi'} &= \check{B} \lh{\xi}
\end{align}
Thus, as already said, these matrices are not representations of multivectors in the strict sense. We can, however, still ask ourselves how these odd down-projections transform under rotor transformations and parity flips.

As one can show by applying a space-time split\footnote{Let $a$ be an odd multivector. Then, $a \gamma_0$ is an even multivector that transforms as $a\gamma_0 \mapsto R a \gamma_0 R^{-1}$. The vector $\gamma_0$ anticommutes with boosts and commutes with rotations, such that if we commute $\gamma_0$ out of this transformation law, the right-hand rotor will undergo a ``parity flip'', such that we obtain the transformation law for odd matrices. We conclude that odd matrices transform like odd multivectors.}, the hermitean parts transform like vectors, while the antihermitean parts transform like pseudovectors. To decompose $\hat{A}, \check{B}$, we make the following definitions:
\begin{align}
	\check{\gamma}_\mu &= (1, P_i), \\
	\hat{\gamma}_\mu &= (1, -P_i)
\end{align}
such that $\{\hat{\gamma}_\mu\}, \{\check{\gamma}_\mu\}$ represent the hermitean/vector parts, and $\{\acute{I} \hat{\gamma}_\mu\}, \{\grave{I} \check{\gamma}_\mu \}$ represent the antihermitean/pseudovector parts. We can thus write:
\begin{align}
	\hat{A} = a^\mu \hat{\gamma_\mu} + b^\mu \acute{I} \hat{\gamma}_\mu \\
	\check{B} = c^\mu \check{\gamma_\mu} + d^\mu \grave{I} \check{\gamma}_\mu
\end{align}

We see that an odd down-projection results in a ``representation'' of the odd part of $\Cl(1, 3)$. This is not a representation in the strict sense, as the odd matrices $\check{A}, \hat{B}$ are not endomorphisms over a single space of spinors. However, they still behave under matrix multiplication in a way that is consistent with the multiplication of the underlying multivectors.

If we multiply $\check{A}$ and $\hat{B}$ with each other, we obtain an even (RH$\rightarrow$RH or LH$\rightarrow$LH) matrix, which represents an even multivector:
\begin{align}
	\check{A} \hat{B} &= \grave{C} \\
	\hat{B} \check{A} &= \acute{D}
\end{align}
Note that this is exactly what we expect from multivector multiplication - two odd multivectors multiplied with each other always give an even multivector. It turns out that this isomorphism is self-consistent - the odd multivectors represented by $\check{A}, \hat{B}$ do indeed multiply to give the even multivectors represented by $\grave{C}$ and $\acute{D}$:
\begin{align}
	\grave{C} &= \grave{(AB)} = \check{A} \hat{B} \\
	\acute{D} &= \acute{(BA)} = \hat{B} \check{A}.
\end{align}
To make this work out, we have intentionally defined $\check{\gamma}_\mu$ and $\hat{\gamma}_\mu$ such that:
\begin{align}
	\Rh{\sigma}_\mu &= \Check{\gamma}_\mu \Hat{\gamma}_0 \\
	\Lh{\sigma}_\mu &= \Hat{\gamma}_\nu \Check{\gamma}_0
\end{align}

We can also multiply an odd matrix onto an even matrix, obtaining an odd matrix:
\begin{align}
	\hat{E} &= \hat{B} \grave{C} \\
	\check{F} &= \check{A} \acute{D}
\end{align}
This multiplication also behaves as we would expect from multiplying the underlying even and odd multivectors:
\begin{align}
	\hat{(BC)} &= \hat{B} \grave{C} \\
	\check{(AD)} &= \check{A} \acute{D}
\end{align}

Therefore, as long as we pay attention to the chiral structure of the matrices, we can represent arbitrary multivector operation with the even and odd matrices. In a way, we can say that we have turned the ``issue'' of spinor chirality into a feature: We started with the $\SL(2, \BC)$ representation of $\Cl^+(1, 3)$, which maps right-handed (left-handed) to right-handed (left-handed) spinors. Then, we considered how maps from and to spinors of unequal chirality look like, and found out that they represent the \textit{odd} part of $\Cl(1, 3)$.

\subsubsection{Chirality flips}
With our newfound knowledge about chiral spinors and down-projections, we can revisit the problem of converting spinors of different chiralities into each other. Previously, we gave the formula (\ref{eq:2_chirality_conversion}) without explaining how we obtained it, but now we are ready to justify it from first principles.

First, we assume that there is a linear map/matrix $C$ covariantly converting right-chiral into left-chiral spinors:
\begin{align}
	\lh{\psi'} = C \rh{\psi}.
\end{align}
This map should convert $\begin{pmatrix} 1 & 0 \end{pmatrix}$ into $\begin{pmatrix} 0 & 1 \end{pmatrix}$ and vice versa, with a possible additional complex phase. This implies that $C$ should look like
\begin{align}
	C = \begin{pmatrix} 0 & e^{i\alpha} \\ e^{i\beta} & 0 \end{pmatrix}.
\end{align}
In addition, it should convert $\begin{pmatrix} 1 & 1 \end{pmatrix}$ into $\begin{pmatrix} 1 & -1 \end{pmatrix}$ and vice versa, again possibly with a complex phase. Therefore,
\begin{align}
	C = e^{i\alpha} \begin{pmatrix} 0 & 1 \\ -1 & 0 \end{pmatrix} = e^{i\alpha} \epsilon.
\end{align}
However, this map has a crucial problem: It is not $\Spin(1, 3)$-covariant - as it is a map from the space of RH to RH spinors, it transforms like an even multivector - specifically, like the even multivector $I e^{I\alpha} \sigma^2$. Singling out the bivector $\sigma^2$ clearly violates $\Spin(1, 3)$ symmetry. However, this $C$ is the only linear map fulfilling the requirements for a chirality flip.

This problem is resolved if we assume $C$ to be an \textit{antilinear} map:
\begin{align}
	\lh{\psi'} = C \rh{\psi}^*.
\end{align}
If we apply this to an odd down-projection, we see that:
\begin{align}
	\lh{\psi'} \rhc{\psi'} = C (\rh{\psi} \lhc{\psi})^* C^T = e^{2i\alpha} \epsilon (\rh{\psi} \lhc{\psi})^* \epsilon^T.
\end{align}
This implies that either $\alpha = 0$ or $\alpha = \pi$. Therefore, the map for spinor chirality flips is
\begin{align}
	\lh{\psi'} = \pm \epsilon \rh{\psi}^*.
\end{align}

\iffalse
Let $\Rh{W}$ denote the space of right-chiral spinors, and $\Lh{W}$ the space of left-chiral spinors. As it is an antilinear map, $\pm \epsilon$ maps from the space of complex-conjugated right-chiral spinors to the left-chiral spinors:
\begin{align}
	\pm \epsilon \in \text{Hom}(\Lh{\bar{W}}, \Rh{W}) = \Rh{W} \otimes \Lh{\bar{W}}^{-1}
\end{align}
where $\bar{L}$ denotes the complex conjugate of a linear space, and $L^{-1}$ its dual. Previously, we have seen that $\Lh{W}^{-1} = \Rh{\bar{W}}$. Therefore, $\Lh{\bar{W}}^{-1} = \Rh{W}$, and
\begin{align}
	\pm \epsilon \in \Rh{W} \otimes \Rh{W}.
\end{align}
In other words, the antilinear map $\pm \epsilon$ converting right-chiral to left-chiral spinors is a rank $(2, 0)$ tensor over the space of right-chiral spinors. Specifically,
\begin{align}
	\pm \epsilon = \pm \left(\rh{+} \otimes \rh{-} - \rh{-} \otimes \rh{+}\right) = \pm \ \rh{+} \wedge \rh{-}. \label{eq:2_chiral_pseudoscalar}
\end{align}
It transforms like:
\begin{align}
	\epsilon \mapsto \Rh{R} \epsilon \Rh{R}^T
\end{align}
The right-hand term looks just like the formula for the determinant of the matrix $\Rh{R}$:
\begin{align}
	\det(\Rh{R}) \epsilon_{ij} = \epsilon_{kl} \Rh{R}^k_{\ i} \Rh{R}^l_{\ j}
\end{align}
As $\Rh{R} \in \SL(2, \BC)$, the group of two-by-two complex matrices with determinant one, we can conclude that $\Rh{R} \epsilon \Rh{R}^T = \epsilon$. Therefore, the antilinear map $\pm \epsilon$ is $\Spin(1, 3)$-covariant.

\iffalse
This, together with (\ref{eq:2_chiral_pseudoscalar}) provides a very interesting interpretation of $\epsilon$: It can be interpreted as a unit pseudoscalar in chiral spinor space, instead of vector space. If we want to convert RH into LH spinors and vice versa, we need to single out such a spinor pseudoscalar - either $+\epsilon$ or $-\epsilon$.
\fi

\subsubsection{Constructing invariants}
Before we go on to construct quantum field theories involving spinors, let us briefly review how we can construct invariants out of chiral spinors.

An even down-projection of two chiral spinors of different chiralities $\rh{\psi}, \lh{\xi}$ will result in an even multivector
\begin{align}
	\grave{M} = \rh{\psi} \rhc{\xi},
\end{align}
of which we can take the scalar, bivector and pseudoscalar part:
\begin{align}
	\langle \rh{\psi} \rhc{\xi} \rangle_0 &\in \laspan\left\{ 1 \right\} \\ %\begin{pmatrix} 1 & 0 \\ 0 & 1 \end{pmatrix} \right\} \\ 
	\langle \rh{\psi} \rhc{\xi} \rangle_2, &\in \laspan\left\{ \gamma_{\mu \nu} \right\} \\ %\sigma^\mu \sigma^\nu, \ \mu \neq \nu \right\} \\ 
	\langle \rh{\psi} \rhc{\xi} \rangle_4, &\in \laspan\left\{ I \right\}
\end{align}

The scalar part is trivially invariant. To calculate it, we take a look at the traces of the respective representations:
\begin{align}
	\trace(\grave{1}) &= 2 \\
	\trace(\grave{\gamma}_{\mu \nu}) &= 0 \\
	\trace(\grave{I}) &= 2i
\end{align}
The scalar part can therefore be calculated by taking the real part of the matrix trace divided by two:
\begin{align}
	\langle \rh{\psi} \rhc{\xi} \rangle_0 &= \frac{1}{2} \Re \trace (\rh{\psi}\rhc{\xi})
%					       &= \frac{1}{2} \Re \langle\grave{\xi} | \grave{\psi} \rangle
\end{align}
If we single out a unit pseudoscalar $I$ (i.e. choose an orientation), we can also construct an invariant out of the pseudoscalar part:
\begin{align}
	\langle \rh{\psi} \rhc{\xi}\rangle_4 I \label{eq:2_pseudoscalar_invariant}
\end{align}
with the explicit formula
\begin{align}
	\langle \rh{\psi} \rhc{\xi} \rangle_4 I = - \frac{1}{2} \Im \trace(\rh{\psi} \rhc{\xi})
\end{align}
This is an important point: Even though both the real and imaginary parts of $\trace(\rh{\psi} \rhc{\xi})$ are invariant under proper orthochronous Lorentz transformations (i.e. invariant under $\Spin^+(1, 3)$), only the real part is an actual scalar - the imaginary part is a pseudoscalar. In parity-conserving field theories, we are not allowed to single out a unit pseudoscalar and thus cannot construct invariants of the form (\ref{eq:2_pseudoscalar_invariant}).

If we happen to have a bivector $B$ at hand, we can form the invariant
\begin{align}
	\langle \rh{\psi} \rhc{\xi} \rangle_2 \cdot B.
\end{align}
Analogously, we can also form invariants with an odd down-projection $\rh{\psi} \lhc{\phi}$. Let $v$ be a vector and $W$ be a pseudovector. Then,
\begin{align}
	&\langle \rh{\psi} \lhc{\phi} \rangle_1 \cdot v \\
	&\langle \rhc{\psi} \lhc{\phi} \rangle_3 \cdot W
\end{align}
are both scalars.

The determinant of any chiral spinor down-projection is also an invariant, as the chiral representation of a rotor always has determinant 1. A particularly interesting case is an odd down-projection $\check{A} = A^\mu \check{\gamma}_\mu$ whose pseudovector component is zero. Its determinant is:
\begin{align}
	\det \check{A} &= \det \begin{pmatrix} A^t + A^z & A^x - iA^y \\ A^x + iA^y & A^t - A^z \end{pmatrix} \\
		       &= (A^t + A^x)(A^t - A^x) - (A^x + iA^y)(A^x - iA^y) \\
		       &= (A^t)^2  - (A^x)^2 - (A^y)^2 - (A^z)^2 \\
		       &= A^2.
\end{align}
If we take a single chiral spinor $\rh{\psi}$ and down-project it to form an odd multivector $\rh{\psi} \lhc{\psi}$, both its pseudovector part and determinant will be zero. Therefore, $\rh{\psi} \lhc{\psi}$ is a null vector. This will turn out to be relevant soon - we will see that free particles described by chiral spinors, so-called \textit{chiral fermions}, necessarily move at the speed of light.
