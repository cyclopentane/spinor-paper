\subsection{Classical matrix-vector formalism}
The classical matrix-vector spinor algebra grew historically with the works of various physicists. Wolfgang Pauli first formulated a non-relativistic theory of spin, which Paul Dirac took as a basis to formulate his relativistic theory of bichiral (Dirac) fermions. In turn, Hermann Weyl showed that Dirac's theory can be viewed as the theory of two coupled chiral fermions - then called Weyl fermions. Owing to these historic subtleties, the standard formalism contains concepts from varying stages of the development of quantum mechanics and thus carries a lot of ``conceptual baggage'' we wish to cast off with our reformulation. In the following, we will use the conventions of \cite{peskin_schroeder_spinors}.

Most of our expressions in which we calculate the down-projection of spinors and then form invariants have direct equivalents once the semantic information is removed from the objects. Consider, for instance, the right-chiral fermion action:
\begin{align}
	\mathcal{L} &= i\dot{\partial} \cdot \expval{\dot{\rh{\psi}} \lhc{\psi}}_1 \\
	&= \expval{i\partial \rh{\psi} \lhc{\psi}} \\
	&\propto \trace(i\hat{\partial} \rh{\psi} \lhc{\psi}) \\
	&\propto \lhc{\psi} i \hat{\partial} \rh{\psi} \\
	&= \lhc{\psi} i \hat{\gamma}^\mu \partial_\mu \rh{\psi}.
\end{align}
Now, if we remove the semantic information embedded in our formalism, we obtain the matrix-vector expression
\begin{align}
	\mathcal{L} &= \psi^\dag i \bar{\sigma}^\mu \partial_\mu \psi,
\end{align}
the well-known ``right-handed Weyl Lagrangian''.

We see that our formalism and the standard formalism are similar regarding their algebraic structure. However, we are placing a focus on semantical structure - that is, the reader should have a thorough understanding of what the matrices and vectors involved in the calculations represent, geometrically. This is something the standard formalism does not do at all - most works on Dirac theory just start by postulating the Dirac matrices and a transformation law, and then showing that it fulfills the Lie algebra of $\SO(1, 3)$. Geometric explanations or proofs about the self-consistency of Dirac's theory are almost never given - and if, they consist of tedious matrix algebra and/or abstract representation theory. Many subtleties like the difference between even and odd down-projections are ignored, and instead resolved through brute-force calculations. Our approach therefore have the advantage of providing a clear and coherent picture from the beginning.


\subsubsection{Dirac gamma matrices and $\Spin(1, 3)$ covariance}
The Dirac gamma matrices are the basic building block of the conventional formalism. They generate a representation ofthe complexified STA. The main difference to our formalism is that we have flipped the signs of the spacelike gamma matrices in order to make the rotor transformation law and the definition of right-chiral and left-chiral spinors consistent.

As the Dirac gamma matrices do not carry any geometric meaning in the formalism, they frequently appear in a way that ostensibly breaks $\Spin(1, 3)$ covariance. For instance, the bichiral conjugate (normally called psi-bar or Dirac conjugate) is frequently defined as
\begin{align}
	\bar{\psi} = \psi^\dag \gamma_0. \label{eq:4_bichiral_conj}
\end{align}
In the context of our formalism, it seems confusing why we would have to single out a timelike unit vector to define the bichiral conjugate, whereas in the standard formalism, this question does not arise because $\gamma_0$ does not have any semantic meaning beyond its definition as an abstract matrix.
\footnote{To answer the question - the hermitean conjugate is not a $\Spin(1, 3)$-covariant operation, and it yields different spinors depending on the frame of reference it was conducted in. To be precise, taking the hermitean conjugate of a spinor amounts to taking its bichiral conjugate (which is a covariant operation) and then parity-flipping it along the time axis of the current coordinate system (which is obviously not a covariant operation). To remedy the parity flip, we simply conduct another one in (\ref{eq:4_bichiral_conj}).}

Similarly, in the standard formalism, the charge conjugate operation is often defined as
\begin{align}
	\psi_C = i \gamma^2 \psi^*.
\end{align}
Technically, our matrix $C$ and the matrix $\gamma^2$ have the same values, but they have completely different meanings. $\gamma^2$ is a multivector, and its representation a (linear) endomorphism over the space of bichiral spinors. In contrast, $C$ maps the space of complex-conjugated spinors onto the space of normal spinors and thus defines an antilinear map over the spinors. $\gamma^2$ just happens to be the correct matrix for that operation with the conventional definitions, similar to how the spacetime metric $\eta_{\mu \nu}$ and the parity-flip matrix for four-vectors have the same components but completely different semantic meanings and transformation properties.

\subsubsection{Dirac bilineals}
In the standard formalism, possible observables and interactions between fermions $\psi, \phi$ are given by the Dirac bilineals. They simply are the various grade of the multivector $\ket{\psi} \bra{\phi}$:
\begin{align}
	\expval{ \ket{\psi} \bra{\phi} }_0 \quad &\longleftrightarrow  \quad \bar{\psi} \phi & \text{(scalar)} \\
	\expval{ \ket{\psi} \bra{\phi} }_1 \quad &\longleftrightarrow \quad \bar{\psi} \gamma_\mu \phi & \text{(vector)} \\
	-i/2 \expval{ \ket{\psi} \bra{\phi} }_2 \quad &\longleftrightarrow \quad \bar{\psi} \frac{i}{2} \gamma_{[\mu} \gamma_{\nu]} \phi & \text{(bivector\footnote{Normally called the tensor bilineal. This is because rank (2, 0) antisymmetric tensors are bivectors.})} \\
	-i\expval{ \ket{\psi} \bra{\phi} }_3 \quad &\longleftrightarrow \quad \bar{\psi} \gamma_\mu \gamma^5 \phi & \text{(pseudovector)} \\
	-i\expval{ \ket{\psi} \bra{\phi} }_4 \quad &\longleftrightarrow \quad \bar{\psi} \gamma^5 \phi & \text{(pseudoscalar)},
\end{align}
where $\gamma^5 = iI$, and we have used square brackets to indicate antisymmetric indices. The factors of $i$ in the last three bilineals are convention, as these grades will be imaginary for the down-projection of a single fermion. The factor of $1/2$ in the bivector is chosen such that we recover the Pauli matrix definition of the spin operator when applying the bivector bilineal for $\gamma_{\mu \nu}$ to a non-relativistic particle.

