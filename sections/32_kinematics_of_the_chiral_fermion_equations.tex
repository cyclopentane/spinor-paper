\subsection{Kinematics of chiral fermions}
Let us now try to find the solutions of the chiral fermion equations. We have seen that the chiral fermion equations imply the massless Klein-Gordon equation. Thus, any solution of the chiral fermion equations must also be a solution to the massless Klein-Gordon equation. In addition, the chiral equations impose additional structure in the spinor component of the fields.

In momentum space, the massless Klein-Gordon equation is:
\begin{align}
	k^2 \ket{\psi} = 0.
\end{align}
This condition can be read as ``Only the field modes $\ket{\psi(k)}$ fulfilling the massless dispersion relation $k^2 = 0$ can be non-zero''. If we only postulated the Klein-Gordon equation, the values of these field modes could be anything:
\begin{align}
	\rh{\psi} &= \rh{u} e^{ip \cdot x} \\
	\lh{\xi} &= \lh{w} e^{ip \cdot x}
\end{align}
where $\rh{u}, \lh{w}$ are arbitrary chiral spinors. With the additional constraint of the momentum-space chiral fermion equation, they need to fulfill:
\begin{align}
	\hat{k}   \rh{u} &= 0 \label{eq:3_weyl_k_rh} \\
	\check{k} \lh{w} &= 0 \label{eq:3_weyl_k_lh}.
\end{align}

The operations here are not geometric products, but matrix-vector multiplications. In theory, we could now mindlessly grind down the resulting equation systems. We can, however, also employ a small trick to make sense of these equations in terms of multivectors - we postmultiply the respective spinors:
\begin{align}
	\hat{k} \rh{u} \lhc{u} &= 0, \label{eq:3_weyl_k_rh_sq} \\
	\check{k} \lh{w} \rhc{w} &= 0. \label{eq:3_weyl_k_lh_sq}
\end{align}
Note that as long as $\grave{u}(k), \acute{u}(k)$ are not zero, these conditions are equivalent to (\ref{eq:3_weyl_k_rh}, \ref{eq:3_weyl_k_lh}). However, they are now multivector expressions - we are multiplying the vector $k$ with an odd projection multivector. The result is an even multivector, which should be equal to zero.

As shown before, an odd down-projection of a single spinor $\rh{\psi} \lhc{\psi}$ always just consists of a null vector. This null vector has to yield zero when multiplied by $k$. The only null vector that fulfills this condition is $k$ (or a scalar multiple of it). Therefore, we can conclude that the respective solutions of the chiral fermion equations need to be chiral spinors that down-project to the vector $k$ or a scalar multiple of it. For instance, the spinor
\begin{align}
	\rh{u} = \begin{pmatrix} 1 \\ 0 \end{pmatrix}
\end{align}
is a solution for all wavemodes with $k \propto \gamma_0 + \gamma_3$, e.g.:
\begin{align}
	\rh{\psi} = \rh{u} e^{ik \cdot x} = \begin{pmatrix} 1 \\ 0 \end{pmatrix} e^{i(\gamma_0 + \gamma_3)\cdot x}
\end{align}
This demonstrates how the properties of chiral spinors directly require that chiral fermions move at the speed of light.

