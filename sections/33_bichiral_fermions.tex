\subsection{Mass couplings and bichiral fermions}
We now investigate what sort of couplings between chiral fermions are allowed. A coupling of a chiral spinor field with itself is forbidden, as we cannot form a scalar out of two chiral spinors of the same chirality.\footnote{The reader might ask themself why we could not flip the chirality of one of the chiral spinors with $\epsilon$ and thus couple a chiral spinor field to itself. In fact, this is possible, and this will result in so-called Majorana mass terms and Majorana fermions. However, they are outside the scope of this paper.} However, a similar coupling between a right-handed and a left-handed chiral fermion $\rh{\psi_R}, \lh{\psi_L}$ is allowed:
\begin{align}
	\mathcal{L}_{\mathrm{int}} = -2m\expval{ \rh{\psi_R} \rhc{\psi_L} },
\end{align}
where $m$ is some real coupling constant, with the factor $-2$ chosen to make our results come out nicely. This is the simplest possible coupling between two chiral fermions; all other invariant coupling terms require additional multivectors or spinors. In principle, we could treat this field theory of left-handed fermions coupling to right-handed fermions in a pertubation-theoretic way. However, there are exact solutions for this Lagrangian. We could manually solve the complicated equation system resulting from the application of the Euler-Lagrange equations, but we can also compactify our formalism significantly to reduce this computation to almost nothing.

\subsubsection{Bichiral spinors}

We start by combining the left-handed and right-handed fields into a \textit{bichiral spinor}\footnote{Also called \textit{Dirac spinor} or \textit{bispinor}.} $\chi$:
\begin{align}
	\ket{\chi} = \begin{pmatrix} \lh{\xi} \\ \rh{\psi} \end{pmatrix}. \label{eq:3_proto_dirac_1}
\end{align}.
This bichiral spinor transforms as
\begin{align}
	\ket{\chi} \mapsto \slashed{R} \ket{\chi},
\end{align}
where
\begin{align}
	\slashed{R} = \begin{pmatrix} \acute{R} & 0 \\ 0 & \grave{R} \end{pmatrix}.
\end{align}
is the \textit{bichiral representation} of the rotor $R$. We see that we can represent an even multivector by putting its left-chiral and right-chiral representations on the two-by-two diagonal blocks of the four-by-four matrix, i.e. taking the direct sum between the left-handed and right-handed chiral representation.

Furthermore, we define the \textit{bichiral conjugate}\footnote{Also called \text{Dirac conjugate} or \textit{psi-bar}, owing to its conventional notation as $\bar{\chi}$.} $\bra{\xi}$ of the bichiral spinor to be:
\begin{align}
	\bra{\chi} = \begin{pmatrix} \lhc{\psi} & \rhc{\xi} \end{pmatrix}
\end{align}
Note that this is not a simple hermitean conjugate - the right-handed and left-handed parts have been flipped, such that the inner product between two bispinors $\ket{\chi}, \ket{\zeta}$ is invariant:
\begin{align}
	\braket{\chi}{\zeta} = \langle \acute{\chi}_R | \acute{\zeta}_L \rangle + \langle \grave{\chi}_L | \grave{\zeta}_R \rangle.
\end{align}
In conventional notation, this operation is denoted by ``$\bar{\psi} = \psi^\dag \gamma_0$''. We will, however, meticulously avoid this notation for reasons that will be discussed later.

We can now form a ``bichiral down-projection'' of two bichiral spinors $\chi, \zeta$:
\begin{align}
	\ket{\chi} \bra{\zeta} &= \begin{pmatrix}
		| \acute{\chi}_L \rangle \langle \acute{\zeta}_R| & | \acute{\chi}_L \rangle \langle \grave{\zeta}_L| \\
		| \grave{\chi}_R \rangle \langle \acute{\zeta}_R| & | \grave{\chi}_R \rangle \langle \grave{\zeta}_L| 
	\end{pmatrix} \label{eq:3_dirac_A}
\end{align}
We see that the bichiral down-projection simply is the combination of all possible even and odd down-projections between the constituent chiral spinors of $\ket{\chi}$ and $\ket{\zeta}$. The diagonal blocks are even down-projections and represent even multivectors, while the off-diagonal blocks are odd down-projections and represent odd multivectors.

So far, we have only defined the bichiral representation for even multivectors. However, the presence of odd matrices in (\ref{eq:3_dirac_A}) suggests that we can also represent odd multivectors with the bichiral representation. For an odd multivector $A_{\text{odd}}$, we might tentatively define
\begin{align}
	\slashed{A}_{\text{odd}} = \begin{pmatrix} 0 & \hat{A}_{\text{odd}} \\ \check{A}_{\text{odd}} & 0 \end{pmatrix},
\end{align}
such that the representation of an arbitrary multivector $A = A_{\text{even}} + A_{\text{odd}}$ is:
\begin{align}
	\slashed{A} = \begin{pmatrix} \acute{A}_{\text{even}} & \hat{A}_{\text{odd}} \\ \check{A}_{\text{odd}} & \grave{A}_{\text{even}} \end{pmatrix} \label{eq:3_dirac_A2}
\end{align}
It is easy to check that this representation is self-consistent by multiplying two such matrices with each other. However, this representation is not surjective. Remember that $\grave{A}_{\text{even}}, \acute{A}_{\text{even}}$ and $\hat{A}_{\text{odd}}, \check{A}_{\text{odd}}$ are related to each other by a parity flip. This can also be seen by counting the degrees of freedom - STA multivectors have 16 degrees of freedom, while complex four-by-four matrices have 32.

However, the space of complex four-by-four matrices is spanned by matrices of the form
\begin{align}
	C = \slashed{A} + \slashed{B} i,
\end{align}
where $i \in \BC$ is the complex imaginary unit, as can be seen by writing down the behaviour of the hermitean and antihermitean components under parity flips and multiplication by $i$. This combination of a real and an imaginary STA multivector fills up the 32 degrees of freedom of the matrix.

This is a very important point - the bichiral representation, is not just a representation of the real space-time algebra $\Cl(1, 3)$, but of the \textit{complexified} STA $\Cl(1, 3, \BC)$. This means that multivectors represented by such matrices can have complex coefficients. In this representation, the basis vectors of $\Cl(1, 3, \BC)$ are given by:\footnote{
This is the same as the representation that is conentionally (confusingly) called ``Weyl representation'', save for a lowered spacetime index $\gamma_\mu = \eta_{\mu \nu} \gamma^\nu$ - the standard formalism does not treat the $\gamma$s as a covariant vector basis $\gamma_\mu$, but as a contravariant four-vector of matrices $\gamma^\mu$ by default.}
\begin{align}
	\slashed{\gamma}_\mu = \begin{pmatrix} 0 & \hat{\gamma}_\mu \\ \check{\gamma}_\mu & 0 \end{pmatrix} = \begin{pmatrix} 0 & \bar{P}_\mu \\ P_\mu & 0 \end{pmatrix},
\end{align}
or explicitly:
\begin{align}
	\slashed{\gamma}_0 &= \begin{pmatrix} 0 & 0 & 1 & 0 \\ 0 & 0 & 0 & 1 \\ 1 & 0 & 0 & 0 \\ 0 & 1 & 0 & 0 \end{pmatrix} \hspace{20pt} 
	\slashed{\gamma}_1 = \begin{pmatrix} 0 & 0 & 0 & -1 \\ 0 & 0 & -1 & 0 \\ 0 & 1 & 0 & 0 \\ 1 & 0 & 0 & 0 \end{pmatrix} \\
	\slashed{\gamma}_2 &= \begin{pmatrix} 0 & 0 & 0 & i \\ 0 & 0 & -i & 0 \\ 0 & i & 0 & 0 \\ -i & 0 & 0 & 0 \end{pmatrix} \hspace{20pt}
	\slashed{\gamma}_3 = \begin{pmatrix} 0 & 0 & -1 & 0 \\ 0 & 0 & 0 & 1 \\ 1 & 0 & 0 & 0 \\ 0 & -1 & 0 & 0 \end{pmatrix}
\end{align}
We do not have to worry about parities anymore, as we can express a parity flip with the matrix transformation $U = \slashed{\gamma}_0$ i.e. the matrix swapping the left-handed and right-handed components of a spinor $\ket{\psi}$:
\begin{align}
	\ket{\chi} \mapsto \slashed{\gamma}_0 \ket{\chi} \label{eq:3_dirac_parity_flip}
\end{align}
This means that all possible bichiral representations are similar to each other. The operation (\ref{eq:3_dirac_parity_flip}) can be interpreted as a passive parity flip, i.e. a parity flip of the spinor representation, while the physical spinor itself stays the same. The other option is an active parity flip - the representation stays the same, but the spinor becomes parity-flipped. Also note that the choice of the $\gamma_0$ matrix is not by accident - (\ref{eq:3_dirac_parity_flip}) implies that bichiral down-projections (and, by consequence, STA multivectors in general) parity-flip as
\begin{align}
	\ket{\chi} \bra{\zeta} &\mapsto \slashed{\gamma}_0 \ket{\chi} \bra{\zeta} \slashed{\gamma}_0 \\
	\Rightarrow M &\mapsto \gamma_0 M \gamma_0,
\end{align}
which is the well-known parity flip law in STA. The reason for why we were not able to express this with chiral representations is that they only represented the even subalgebra, while $\gamma_0$ is an element of the odd part of the STA.

We see that we do not have to care about the specifics of our representations anymore. Therefore, we will drop the slashes for readability reasons from now on.

\subsubsection{Bichiral fermion equation}
We are now equipped to join the left-handed and right-handed parts of our Lagrangian into a single bichiral expression:
\begin{align}
	\mathcal{L} &= \expval{ p \ \rh{\psi_R} \lhc{\psi_R} }
                    + \expval{ p \ \lh{\psi_L} \rhc{\psi_L} }
	           -2m \expval{ \rh{\psi_R} \rhc{\psi_L}} \\
		    &= \expval{p \ \begin{pmatrix} 0 & \lh{\psi_L} \rhc{\psi_L} \\ \rh{\psi_R} \lhc{\psi_R} & 0 \end{pmatrix} } - m \expval{ \begin{pmatrix} \lh{\psi_L} \lhc{\psi_R} & 0 \\ 0 & \rh{\psi_R} \rhc{\psi_L} \end{pmatrix} } \\
		    &= \expval{ (p - m) \begin{pmatrix} \lh{\psi_L} \lhc{\psi_R} & \lh{\psi_L} \rhc{\psi_L} \\ \rh{\psi_R} \lhc{\psi_R} & \rh{\psi_R} \rhc{\psi_L} \end{pmatrix} } \\
		    &= \expval{ (p - m) \ket{\psi} \bra{\psi} },
\end{align}
or in position space:
\begin{align}
	\mathcal{L} = \expval{ (i \partial - m) \ket{\psi} \bra{\psi} }.
\end{align}
in position space. This is the bichiral fermion action. It implies the bichiral fermion equation\footnote{Conventionally called Dirac equation. The particles described by it are normally called Dirac fermions.}
\begin{align}
	(i \partial - m) \ket{\psi} = 0.
\end{align}
In principle, this is nothing but a restatement of the original coupled two-particle action. As we will see in the next section, it is however much more convenient to think of $\ket{\psi}$ as describing one single particle, a \textit{bichiral fermion}, instead of as two coupled chiral fermions. Technically speaking, the resulting bichiral is a superposition of the right-chiral and left-chiral components, as depicted in Figure \ref{fig:bichiral velocity}.

\begin{figure}
	\centering
	\begin{tikzpicture}
		\draw[->, thick] (-3, 0) -- (3, 0) node[anchor=west] {$\gamma_3$};
		\draw[->, thick] (0, -3) -- (0, 3) node[anchor=west] {$\gamma_0$};
		\draw[dotted] (-3, -3) -- (3, 3);
		\draw[dotted] (3, -3) -- (-3, 3);
		\draw[->, violet] (0, 0) -- (1, 3) node[midway, anchor=west]{$p$};
		\draw[->, blue] (0, 0) -- (2, 2) node[anchor=west]{$p_R$};
		\draw[->, red] (2, 2) -- (1, 3) node[midway, anchor=west]{$p_L$};
		\draw[->, blue] (-1, 1) -- (1, 3);
		\draw[->, red] (0, 0) -- (-1, 1);
	\end{tikzpicture}
	\caption{A bichiral fermion (violet) is a superposition between a right-chiral (blue) and a left-chiral (red) fermion. Its four-momentum $p$ is the sum of the momenta of its constituent chiral fermions $p_R, p_L$. As the constituent momenta are lightlike, the resuling momentum is timelike. In this diagram, the spin of the fermion(s) is pointing in the $\gamma_3$ direction.}
	\label{fig:bichiral velocity}
\end{figure}
