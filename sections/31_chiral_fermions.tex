\subsection{Chiral fermions}
We start by postulating a chiral spinor field $|\grave{\psi}(x)\rangle$. A reasonably symmetric action $S = \int \dd^4 x \  \mathcal{L}$ for this field should be
\begin{itemize}
	\item a real scalar,
	\item local and translation-invariant, i.e. conserve momentum,
	\item parity-invariant.
\end{itemize}
In order to fulfill conservation of momentum, we will work in momentum space, i.e. take the Fourier transform
\begin{align}
	|\grave{\psi}(x)\rangle =: \int \frac{\dd^4 p}{(2\pi)^4} \ |\grave{\psi}(p) \rangle e^{-ip\cdot x},
\end{align}
and construct the action density from it. Then, we can directly read off conservation of momentum from whether the incoming momenta of a term sum to zero.

As we want to make the spinor field dynamic, we need to link its values to the spacetime it is defined on somehow. We therefore form the odd down-projection $| \grave{\psi}(p) \rangle \langle \acute{\psi}(q)|$ to spin-1 space, where $p$ and $q$ are two different field momenta. If we add no further wavelength modes to the product, the requirement for momentum conservation translates to $q = -p$. We therefore arrive at the odd multivector
\begin{align}
	|\grave{\psi}(p) \rangle \langle \acute{\psi}(-p)|. \label{eq:3_psi_dp_1}
\end{align}
Right now, the only other multivector at our disposal is the momentum four-vector $p$. Therefore, the easiest way to build a scalar from $\grave{\psi}$ is to take the scalar product between the vector part of (\ref{eq:3_psi_dp_1}) and $p$:
\begin{align}
	\mathcal{L} &= p \cdot \expval{ |\grave{\psi}(p) \rangle \langle \acute{\psi}(-p) | }_1 \\
		    &= \expval{ p \ \rh{\psi} \lhc{\psi} }
\end{align}
This is the well-known right-chiral fermion action density. The above derivation can be analogously performed for a left-chiral spinor field $|\acute{\xi}(x)\rangle$ - this yields a action density of
\begin{align}
	\mathcal{L} = \expval{ p \ \lh{\xi} \rhc{\xi} }
\end{align}
We can also translate these action densities into position space:
\begin{align}
	\mathcal{L}_{\text{RH}} &= \expval{i\partial \rh{\psi}{\lhc{\psi}}} \\
	\mathcal{L}_{\text{LH}} &= \expval{i\partial \lh{\xi}{\rhc{\xi}}}
\end{align}
where the vector derivatives are only acting on the first spinor, respectively.

We will call the particles described by such a quantum field \textit{chiral fermions}. They obey the chiral fermion equations\footnote{Conventionally called Weyl fermions and equations, respectively.}
\begin{align}
	\hat{\partial} \rh{\psi} &= 0 \\
	\check{\partial} \lh{\psi} &= 0.
\end{align}
as can be seen by applying the Euler-Lagrange equation. To investigate their behaviour, we premultiply them with $\check{\partial}$ and $\hat{\partial}$ respectively:\footnote{Note that we need to premultiply $\check{\partial}$ onto $\hat{\partial}$ and $\hat{\partial}$ onto $\check{\partial}$ - else, the resulting expressions would not be covariant. As discussed, the result is an even multivector operator - or more specifically, a scalar operator.}
\begin{align}
	\check{\partial} \hat{\partial} \rh{\psi} &= \partial^2 \rh{\psi} = 0 \\
	\hat{\partial} \check{\partial} \lh{\psi} &= \partial^2 \lh{\psi} = 0.
\end{align}
We see that chiral fermions also obey the massless Klein-Gordon equation. Hence, chiral fermions are necessarily massless.

This derivation also shows how the down-projections we are forming from spinors are roughly analogous to the density matrices from quantum mechanics - however, as already said, we will not use the viewpoint of states, observables (and density matrices) in this paper.
