\subsection{Vectors and ellipsoids}
In this section, we will discuss how the transformation properties of vectors and quadratic forms relate to each other. We are going to show how vectors are spin-1 objects, while quadratic forms are spin-2. For didactical reasons, we are going to use the matrix-vector formalism for describing rotations in this section.

Vectors transform under the one-sided action of a rotation matrix $M \in SO(3)$:
\begin{align}
	v \mapsto M v
\end{align}

Three-dimensional bilinear forms $\beta: (v, w) \mapsto v^T \beta w$ transform under the two-sided action of the $SO(3)$ group:
\begin{align}
	\beta \mapsto M \beta M^T \label{eq:bilinear matrix transformation},
\end{align}
such that $v^T \beta w$ is invariant under rotations.

Symmetric bilinear forms are also called quadratic forms, and correspond to symmetric matrices ($\beta = \beta^T$). The most prominent example of such a field is the gravitational field from standard general relatity - the metric is a quadradic form, and the metric field transforms under the two-sided action of $\SO(1, 3)$.

If we write a rotation matrix $M$ as an exponential $M = \exp(\omega^i T_i)$, we can express the transformation law infinitesimally
\begin{align}
	\beta \mapsto \beta + \omega^i (T_i \beta + \beta T_i^T) \label{eq:bilinear transformation law}
\end{align}
If we assume that $\beta$ is symmetric, we can rewrite this to
\begin{align}
	\beta \mapsto \beta + \omega^i \left( T_i \beta + (T_i \beta)^T \right).
\end{align}
A general quadratic form can be parametrized as
\begin{align}
	\beta = \begin{pmatrix} \beta_{11} & \beta_{12} & \beta_{13} \\ \textcolor{gray}{\beta_{12}} & \beta_{22} & \beta_{23} \\ \textcolor{gray}{\beta_{13}} & \textcolor{gray}{\beta_{23}} & \beta_{33} \end{pmatrix}.
\end{align}
It can be constructed from the sum of outer products of vectors:
\begin{align}
	\beta = &\beta_{11} \hat{x} \hat{x}^T + \beta_{22} \hat{y} \hat{y}^T + \beta_{33} \hat{z} \hat{z}^T\\
	+ &\beta_{12} \left( \hat{x} \hat{y}^T + \hat{y} \hat{x}^T \right) + \beta_{13} \left( \hat{x} \hat{z}^T + \hat{z} \hat{x}^T \right) + \beta_{23} \left( \hat{y} \hat{z}^T + \hat{z} \hat{y}^T \right).
\end{align}
Crucially, we can take two arbitrary vectors $v, w$ and form a new quadratic form $\beta = vw^T + wv^T$ out of them. We will call such an operation a ``down-projection'' of $v$ and $w$ into spin-2 space. The reason for this odd choice of words will become clear soon.

Alternatively, we can reshape $\beta$ to a column matrix (``vector''):
\begin{align}
	\bar{\beta} = \begin{pmatrix} \beta_{11} & \beta_{12} & \beta_{13} & \beta_{22} & \beta_{23} & \beta_{33} \end{pmatrix}^T.
\end{align}
Because (\ref{eq:bilinear transformation law}) is a linear operation, we can write it as a single matrix multiplication in terms of $\bar{\beta}$:
\begin{align}
	\bar{\beta} \mapsto \bar{\beta} + \omega^i \bar{T}_i \bar{\beta}
\end{align}
Every one of these generators $\bar{T}_i$ has eigenvalues $\pm 2, \pm 1$, and $0$ (the reader is encouraged to verify this by themself). This means that a quadratic form field has spin 2 and can take spin values of $\pm 2, \pm 1$ and $0$. In representation-theoretic language, we say that 3D quadratic forms transform under the symmetric part of the $\SO(3) \otimes \SO(3)$ representation of the rotor group, which is a spin-2 representation.

We now turn towards a geometric picture for these abstract transformation properties. A quadratic form can be visualized as the quadric surface
\begin{align}
	\left\{ x \in V, \beta^{-1}(x, x) = 1 \right\}
\end{align}
i.e. an ellipsoid/hyperboloid. The principal axes of the quadric correspond to the eigenvectors of $\beta$, while the axes lengths corresponding to the square roots of the eigenvalues. Investigating the precise transformation properties of these hyperboloids in terms of the elementary tensors formed from the eigenvectors of $T_i$ is an interesting study in representation theory, but beyond the scope of this paper.

\begin{figure}
	\centering
	\begin{tikzpicture}
		\node at (0, 3.5) {$\beta = \textcolor{red}{\begin{pmatrix} 2 \\ 2 \end{pmatrix} \begin{pmatrix} 2 \\ 2 \end{pmatrix}^T} + \textcolor{blue}{\begin{pmatrix} 1 \\ -1 \end{pmatrix} \begin{pmatrix} 1 \\ -1 \end{pmatrix}^T = \textcolor{violet}{\begin{pmatrix} 5 & 3 \\ 3 & 5 \end{pmatrix}}$};
		\draw[->, thick] (-1.8, 0) -- (1.8, 0) node[anchor=west]{$x$};
		\draw[->, thick] (0, -1.8) -- (0, 1.8) node[anchor=south]{$y$};
		\foreach \x in {-1, -0.5, ...,  1} {
			\draw[thin, dotted] (\x, -1.5) -- (\x, 1.5);
			\draw[thin, dotted] (-1.5, \x) -- (1.5, \x);
		}
		\draw[thick, violet, rotate=45] (0, 0) ellipse (1.414 and 0.707);
		\draw[thick, red, ->] (0, 0) -- (1, 1);
		\draw[thick, blue, ->] (0, 0) -- (0.5, -0.5);
	\end{tikzpicture}
	\caption{Positive-definite quadratic forms correspond to ellipsoids. If some eigenvalues are negative, the quadric surface will be a hyperboloid instead, while null eigenvalues will result in flat surfaces on the origin.}
\end{figure}

Instead, we will investigate a special case: A given hyperboloid $\beta$ is being rotated along a bivector $B = B^i I e_$, with two of its three principal axes $a, b$ parallel to $B$. These two principal axes have unequal lengths. The third principal axis is a vector $c$ perpendicular to $B$. Thus, $\beta$ can be constructed as
\begin{align}
	\beta = a a^T + b b^T + c c^T.
\end{align}
For simplicity, let $a$ and $b$ be right-hand circularly polarized vectors, such that they are eigenvectors of $B^i T_i$ with eigenvalue $1$. In contrast $c$ is an eigenvector with eigenvalue $0$ (longitudinal polarization). These types of quadratic forms are the spatial part of the transverse basis states of gravitational waves. An infinitesimal transform of $\beta$ therefore reads
\begin{align}
	\beta &= \epsilon \left(B (a a^T + b b^T + c c^T) + (a a^T + b b^T + c c^T)B^T \right) \\
	      &= 2i \epsilon (a a^T + b b^T). \label{eq:special bilinear trf law}
\end{align}
% Todo: check hermitean conjugate

The factor of $2$ is important - quadratic forms are symmetric rank 2 tensors over the linear space of vectors and thus transform with two rotation matrices, instead of one. In the special case we have chosen, this directly corresponds to an additional factor of 2 and thus a spin of 2. We can now ask ourselves what happens once we rotate $\beta$ by a non-infinitesimal angle. Exponentiating the infinitesimal transformation law (\ref{eq:special bilinear trf law}) yields
\begin{align}
	\beta'(\theta) = e^{2i\theta} ( a a^T + b b^T ) + c c^T.
\end{align}
for a rotation angle $\theta$.

\begin{figure}
	\centering
	\begin{tikzpicture}
		\begin{scope}[shift={(-4, 0)}]
			\draw[->] (-1.6, 0) -- (1.6, 0);
			\draw[->] (0, -1.6) -- (0, 1.6);
			\draw[thick, violet] (0, 0) ellipse (1.25 and 0.7);
			\draw[->, red, thick] (0, 0) -- (1.25, 0);
			\draw[->, blue, thick] (0, 0) -- (0, 0.7);
		\end{scope}

		\draw[->] (-1.6, 0) -- (1.6, 0);
		\draw[->] (0, -1.6) -- (0, 1.6);
		\draw[thick, violet, dotted] (0, 0) ellipse (1.25 and 0.7);
		\draw[thick, violet, rotate=60] (0, 0) ellipse (1.25 and 0.7);
		\draw[->, thick] (1.4, 0) arc [start angle=0, delta angle=60, radius=1.4];
		\draw[->, red, thick, rotate=60] (0, 0) -- (1.25, 0);
		\draw[->, blue, thick, rotate=60] (0, 0) -- (0, 0.7);

		\begin{scope}[shift={(4, 0)}]
			\draw[->] (-1.6, 0) -- (1.6, 0);
			\draw[->] (0, -1.6) -- (0, 1.6);
			\draw[thick, violet, dotted, rotate=60] (0, 0) ellipse (1.25 and 0.7);
			\draw[thick, violet, rotate=180] (0, 0) ellipse (1.25 and 0.7);
			\draw[->, red, thick, rotate=180] (0, 0) -- (1.25, 0);
			\draw[->, blue, thick, rotate=180] (0, 0) -- (0, 0.7);
			\draw[->, thick, rotate=60] (1.4, 0) arc [start angle=0, delta angle=120, radius=1.4];
		\end{scope}
	\end{tikzpicture}
	\caption{The vectors defining the principal axes of the ellipse are $360^\circ$-symmetric, but the ellipse itself is $180^\circ$-symmetric.}
	\label{fig:ellipse symmetry}
\end{figure}

The principal axis $c$ is perpendicular to the plane of rotation and thus unaffected by it. The other part of $\beta$, however, transforms with a factor of $e^{2i\theta}$. This is particularly interesting, as the factor of $2$ in the exponent means that the ellipsoid already returns to its original state after a rotation by $180^\circ$ (see Figure \ref{fig:ellipse symmetry}). This type of phenomenon is called \textit{angle doubling}. It is easy to see why this is the case - after a $180^\circ$ rotation, $a$ and $b$ flip signs, so that the respective quadratic forms built from them transform as
\begin{align}
	a a^T \mapsto (-a) (-a^T) = a a^T
\end{align}

This point is important and needs to be emphasized: Vectors by themselves are spin-1 objects, which return back to their original state after a $360^\circ$ rotation. However, we can construct a quadratic form out of a vector $v$ by taking the outer product of the vector with itself, $v v^T$. This object transforms with two rotation matrices instead of one and thus transforms twice as fast as a single vector. This causes it to return to its original state after $180^\circ$ degrees already, exhibiting angle doubling.

Another way to view this: The operation $a \mapsto aa^T$ is a ``down-projection'' from spin-1 to spin-2 space, in which information is lost. There are two vectors $v$, $-v$ that both correspond to the same down-projection $v v^T = (-v) (-v)^T$. The angle doubling phenomenon is inherently connected to this information loss - intuitively speaking, if an object contains less information, it will be more symmetric.

This principle becomes even clearer if we allow parity flips. Let $M \in \O(3)$, i.e. a rotation with an optional parity flip. The transforms $M$ and $-M$ are obviously distinct from each other, as they map a vector $v$ to two different new vectors $Mv$ and $-Mv$. However, when they are applied to a quadratic form $\beta$, it will transform like
\begin{align}
	\beta &\mapsto M \beta M^T \\
	\beta &\mapsto (-M) \beta (-M)^T = M \beta M^T
\end{align}
respectively. We can see that $M$ and $-M$ correspond to the same transformation when applied to quadratic forms - parity flips have no effect on them. Geometrically, this can be seen from the pointwise symmetry of the quadric surfaces around the origin. In representation theory language, we can say that the quadratic form representation of $\O(3)$ is isomorphic to $\SO(3)$. From this point of view, $\SO(3)$ is a \textit{double cover} of $\O(3)$, as both $M \in \O(3)$ and $-M \in \O(3)$ can be identified with $M \in \SO(3)$ for the purpose of transforming quadratic forms.

\iffalse
In this section, we have investigated the relationship between vectors $a$ and quadratic forms. After defining vector fields to be spin-1, we have seen that the double-sided transformation law of quadratic forms ($\beta \mapsto M \beta M^T$) causes them to have spin-2. This factor of 2 in Noether's theorem can be seen explicitly when we consider the rotation of an ellipsoid along two of its three principal axes. In this context, we can see an angle doubling phenomenon - ellipsoids transform ``twice as fast'' as vectors. We have also seen that the orthogonal group $\O(3)$ under which vectors transform is a double cover of the $\SO(3)$ group, under which quadratic forms transform.
\fi
